var express         = require('express');
var app             = express();
var path            = require('path');
var fs         = require('fs');
var port       = process.env.PORT || 8008;                     // set our por

var __staticPath = path.join(__dirname,'dist'); // dev path; use ng build --output-path=[folderName]/
// console.log(path.join(__dirname));


const forceSSL = function() {
  return function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(
       ['https://', req.get('Host'), req.url].join('')
      );
    }
    next();
  }
}

// app.get('/*', (req, res) => {
//   res.sendFile(path.join(__dirname, 'dist/index.html'));
// });

app.use('/' , express.static(__staticPath));
app.use('/*', express.static(__staticPath));
app.use(forceSSL())
app.listen(port);

console.log('\n===================================');
console.log('Angular 2 Application hosted on:  ' + port);
// console.log('       RESTful API Endpoints on:  ' + port + ' /api');
console.log('===================================\n');
