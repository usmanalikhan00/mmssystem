import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { LoginService } from  '../../services/login.services';
import { SchoolService } from  '../../services/schools.services';

import * as _ from 'lodash';

@Component({
    selector: 'login',
    providers: [FormBuilder, LoginService, SchoolService],
    templateUrl: 'login.html',
    styleUrls: ['login.css']
})

export class Login {
  
  public errorMsg = '';
  loginForm: FormGroup;

  toggle: boolean = false

  loginerror: boolean = false
  loggedUser: any = null

  userSchool: any = null
  allSchools: any = []

  constructor(private _formBuilder: FormBuilder,
              private _loginService: LoginService, 
              private _schoolService: SchoolService, 
              private _router: Router) {
    this._buildLoginForm();

    // if(document.getElementById("testScript"))
    //   document.getElementById("testScript").remove();
    // var testScript = document.createElement("script");
    // testScript.setAttribute("id", "testScript");
    // testScript.setAttribute("src", "../assets/plugins/morris/morris.min.js");
    // document.body.appendChild(testScript);

    // if(document.getElementById("testScript2"))
    //   document.getElementById("testScript2").remove();
    // var testScript2 = document.createElement("script");
    // testScript2.setAttribute("id", "testScript2");
    // testScript2.setAttribute("src", "../assets/pages/jquery.dashboard.js");
    // document.body.appendChild(testScript2);


  }

  private _buildLoginForm(){
    this.loginForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      role: ['admin', Validators.required],
    });
  }

  ngOnInit(){
    // this.loggedUser = JSON.parse(localStorage.getItem('user'))
    // if (this.loggedUser)
      // this._router.navigate(['/dashboard'])
  }


  getCredentials(values){
    this._loginService.getLoggedInUser(values.email, values.password)
    .subscribe(user => {
      console.log("VALUES AFTER SUCCESS LOGIN:--\n", user)
      this.loggedUser = user.user
      if (user.status){
        this.loginerror = false
        var adminNav = [
                        {link:'dashboard', name:'Dashboard', icon:'zmdi zmdi-view-dashboard'},
                        {link:'schools', name:'Schools', icon:'fa fa-building'},
                        {link:'teachers', name:'Teachers', icon:'zmdi zmdi-accounts-alt'},
                        {link:'students', name:'Students', icon:'zmdi zmdi-accounts-alt'},
                       ]
        var schoolNav = [
                        {link:'dashboard', name:'Dashboard', icon:'zmdi zmdi-view-dashboard'},
                        {link:'students', name:'Students', icon:'zmdi zmdi-accounts-alt'},
                        {link:'teachers', name:'Teachers', icon:'zmdi zmdi-accounts-alt'},
                        {link:'classes', name:'Classes', icon:'fa fa-graduation-cap'},
                        {link:'subjects', name:'Subjects', icon:'fa fa-book'},
                        {link:'feeslips', name:'Fee Slips', icon:'zmdi zmdi-receipt'},
                        {link:'planner', name:'Planner', icon:'fa fa-calendar'},
                        {link:'inbox', name:'Inbox', icon:'fa fa-inbox'},
                        ]
        if (this.loggedUser.roleId === 1){
          this.loggedUser.role = 'admin'
          this.loggedUser.navbar = adminNav
          this.loggedUser.schoolId = null
          localStorage.setItem('user', JSON.stringify(this.loggedUser))
          // console.log("ADMIN")
          // console.log("LOACL USER:---", JSON.parse(localStorage.getItem('user')))
          this._router.navigate(['/dashboard'])
            
        }else if (this.loggedUser.roleId === 2){
          this.loggedUser.role = 'school'
          this.loggedUser.navbar = schoolNav
          this._schoolService.allSchools()
          .subscribe(schools => {
            this.allSchools = schools.schools
            const key = _.filter(this.allSchools, {userId: this.loggedUser.id})
            this.userSchool = key[0]
            this.loggedUser.schoolId = this.userSchool.id
            localStorage.setItem('user', JSON.stringify(this.loggedUser))
            // console.log("SCHOOL")
            // console.log("***** ALL SCHOOLs *****", this.allSchools, 
            //             "\n***** USER SCHOOLs *****", this.userSchool)
            // console.log("LOACL USER:---", JSON.parse(localStorage.getItem('user')))
            this._router.navigate(['/dashboard'])
          })

        }


      }else{
        this.loginerror = true
      }
    })
  }

  // showRightBar(){
  //   (document.querySelector('.right-bar') as HTMLElement).style.display="block"
  //   console.log("RIGHT BAR")
  // }



}