import {Component, ElementRef, ViewChild} from '@angular/core';
import { Router } from  '@angular/router';
import { SchoolService } from  '../../../services/schools.services';
import { StudentsService } from  '../../../services/students.services';
import { FeeslipsService } from  '../../../services/feeslips.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
    selector: 'add-fee-slip',
    providers: [FormBuilder, 
                SchoolService,
                FeeslipsService,
                StudentsService],
    templateUrl: 'addfeeslip.html',
    styleUrls: ['addfeeslip.css']
})

export class Addfeeslip {

  toggle: boolean = false
  loggedUser: any = null
  feeTotal: any = null
  feeTax: any = null
  expenseCount: any = null
  expenseFlag: boolean = false

  feeSlipForm: FormGroup;

  allStudents: any = []
  expenses: any = []
  alerts: any = []
  selectedStduent: any = []
  recurringDays: any = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14',
                        '15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
  dropdownStudent: any = {}
  // dropdownRecurringDays: any = {}
  // dropdownRecurringDays = {
  //   singleSelection: true,
  //   idField: 'id',
  //   textField: 'name',
  //   selectAllText: 'Select All',
  //   unSelectAllText: 'UnSelect All',
  //   itemsShowLimit: 3,
  //   allowSearchFilter: true
  // };
  
  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService,
              private _studentsService: StudentsService,
              private _feeslipsService: FeeslipsService,
              private _router: Router) {
    this._buildFeeSlipForm()
  }

  _buildFeeSlipForm(){
    this.feeSlipForm = this._formBuilder.group({
      student: ['', Validators.required],
      title: ['', Validators.required],
      nationality: ['', Validators.required],
      submitdate: ['', Validators.required],
      amount: ['', Validators.required],
      tax: ['', Validators.required],
      recurring: [false],
      recurringdate: ['0', Validators.required],
    })
  }

  ngOnInit(){
    var self = this;
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("FEE SLIP STARTED:--\n", this.loggedUser)
    this.getAllStudents()
  }

  addExpense(){
    const object = {
      'title': '',
      'value': '',
    }
    this.expenses.push(object)
  }

  removeItem(i){
    var sum = 0
    console.log("ITEM TO REMOVE:---\n", i)
    this.expenses.splice(i, 1)
    // this.feeTotal = $event.srcElement.value
    for (let item of this.expenses){
      if (item.value)
        sum = sum + parseInt(item.value)
    }
    this.expenseCount = sum
  }

  changeTax($event){
    this.feeTax = $event.srcElement.value * 0.01
    console.log("CHANGE TAX:--\n", $event.srcElement.value, this.feeTax * this.feeTotal)
    // this.feeTotal = this.feeTax
  }

  changeFeeAmount($event){
    console.log("CHANGE FEE AMOUNT:--\n", $event.srcElement.value)
    this.feeTotal = $event.srcElement.value
  }

  changeExpensePrice($event){
    var sum = 0
    // this.feeTotal = $event.srcElement.value
    for (let item of this.expenses){
      if (item.value)
        sum = sum + parseInt(item.value)
    }
    this.expenseCount = sum
    console.log("EXPENSE PRICE:--\n", $event.srcElement.value, "\n", sum)
  }



  getAllStudents(){
    this._studentsService.allStudentsBySchool(this.loggedUser.schoolId)
    .subscribe(students => {
      this.allStudents = students.students
      console.log("ALL STUDENTS FROM SCHOOL:--\n", this.allStudents, students)
      // this.selectedStduent = []
      this.dropdownStudent = {
        singleSelection: true,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    })
  }

  addFeeSlip(values){
    this.expenseFlag = false
    if (this.expenses.length){
      for (let obj of this.expenses){
        if (obj.title === '' || obj.amount === ''){
          this.expenseFlag = true;
          break;
        }
      }
    }else{
      values.expenses = this.expenses
    }
    if (!this.expenseFlag){
      values.schoolId = this.loggedUser.schoolId
      values.feeSlipFields = this.expenses
      values.studentId = values.student[0].id
      values.submissionDate = values.submitdate
      values.studentNationality = values.nationality
      values.isRecurring = values.recurring
      values.recurringDay = values.recurringdate
      values.totalAmount = values.amount
      values.tax = values.tax
      console.log("FEE SLIP FORM VALUES:--\n", values)
      this._feeslipsService.addFeeSlip(values)
      .subscribe(result => {
        console.log("RESULT FROM ADD FEE SLIP:--\n", result)
        if (result.status){
          this.feeSlipForm.reset()
          this.expenses = []
          this.alerts = []
          this.alerts.push({
            type: 'success',
            msg: `FEESLIP ADDED SUCCESSFULLY!!`,
            timeout: 1000
          });
        }else{
          this.alerts = []
          this.alerts.push({
            type: 'danger',
            msg: `Something Went Wrong!!`,
            timeout: 1000
          });
        }
      })
    }else{
      this.alerts = []
      this.alerts.push({
        type: 'danger',
        msg: `Please Fill Expenses!!`,
        timeout: 1000
      });      
    }
    // this.feeSlipForm.reset()

  }

  goBack(){
    this._router.navigate(['/feeslips'])
  }

}