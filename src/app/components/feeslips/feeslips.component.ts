import {Component, ElementRef, ViewChild} from '@angular/core';
import { Router } from  '@angular/router';
import { SchoolService } from  '../../services/schools.services';
import { FeeslipsService } from  '../../services/feeslips.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
    selector: 'fee-slips',
    providers: [FormBuilder, SchoolService, FeeslipsService],
    templateUrl: 'feeslips.html',
    styleUrls: ['feeslips.css']
})

export class Feeslips {

  toggle: boolean = false
  loggedUser: any = null
  allFeeSlips: any = []

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  
  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService,
              private _feeslipsService: FeeslipsService,
              private _router: Router) {
  }

  ngOnInit(){
    var self = this;
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("FEE SLIP STARTED:--\n", this.loggedUser)
    this.getAllFeeSlips()
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

  }

  getAllFeeSlips(){
    this._feeslipsService.allFeeSlipsBySchool(this.loggedUser.schoolId)
    .subscribe(feeslips => {
      this.allFeeSlips = feeslips.feeSlips
      console.log("ALL FEE SLIPS:--\n", this.allFeeSlips)
      this.dtTrigger.next()
    })
  }

  editSlip(slip){
    // console.log("SLIP TO EDIT:---\n", slip)
    this._router.navigate(['/editfeeslip', slip.id])
  }

  showAddForm(){
    this._router.navigate(['/addfeeslip'])
  }

  goBack(){
    this._router.navigate(['/dashboard'])
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}