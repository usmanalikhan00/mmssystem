import {Component, ElementRef, ViewChild} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { SchoolService } from  '../../../services/schools.services';
import { StudentsService } from  '../../../services/students.services';
import { FeeslipsService } from  '../../../services/feeslips.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import * as _ from "lodash";

@Component({
    selector: 'edit-fee-slip',
    providers: [FormBuilder, 
                SchoolService,
                FeeslipsService,
                StudentsService],
    templateUrl: 'editfeeslip.html',
    styleUrls: ['editfeeslip.css']
})

export class Editfeeslip {

  toggle: boolean = false
  loggedUser: any = null
  feeTotal: any = null
  feeTax: any = null
  expenseCount: any = null
  expenseFlag: boolean = false

  feeSlipForm: FormGroup;

  allStudents: any = []
  expenses: any = []
  alerts: any = []
  selectedStduent: any = []
  recurringDays: any = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14',
                        '15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
  dropdownStudent: any = {}

  feeslipId: any = null
  feeslipToEdit: any = null
  recurringday: any = null
  
  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService,
              private _studentsService: StudentsService,
              private _feeslipsService: FeeslipsService,
              private _activatedRouter: ActivatedRoute,
              private _router: Router) {
    this._buildFeeSlipForm()
  }

  _buildFeeSlipForm(){
    this.feeSlipForm = this._formBuilder.group({
      student: ['', Validators.required],
      title: ['', Validators.required],
      nationality: ['', Validators.required],
      submitdate: ['', Validators.required],
      amount: ['', Validators.required],
      tax: ['', Validators.required],
      recurring: [false],
      recurringdate: [0, Validators.required],
    })
  }

  ngOnInit(){
    var self = this;
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    // console.log("FEE SLIP STARTED:--\n", this.loggedUser)

    this._activatedRouter.params.subscribe(params => {
      this.feeslipId = params['feeslipId']; // (+) converts string 'id' to a number
      this._feeslipsService.getFeeSlipById(this.feeslipId)
      .subscribe(result => {
        this.feeslipToEdit = result.feeSlip
        this.expenses = this.feeslipToEdit.feeslipFields
        this.recurringday = this.feeslipToEdit.recurringDay
        console.log("FEE SLIP TO EDIT:-----\n", this.feeslipToEdit, result)
        this.getAllStudents()
        this.setForm()
      })
    });
  }

  addExpense(){
    const object = {
      'title': '',
      'value': '',
    }
    this.expenses.push(object)
  }

  removeItem(i){
    console.log("ITEM TO REMOVE:---\n", i)
    this.expenses.splice(i, 1)
    // this.feeTotal = $event.srcElement.value
    this.countExpenses()
  }

  countExpenses(){

    // debugger;
    var sum = 0
    for (let item of this.expenses){
      if (item.value)
        sum = sum + parseInt(item.value)
    }
    this.expenseCount = sum
  
  }

  changeTax($event){
    this.feeTax = $event.srcElement.value * 0.01
    console.log("CHANGE TAX:--\n", $event.srcElement.value, this.feeTax * this.feeTotal)
    // this.feeTotal = this.feeTax
  }

  changeFeeAmount($event){
    console.log("CHANGE FEE AMOUNT:--\n", $event.srcElement.value)
    this.feeTotal = $event.srcElement.value
  }

  changeExpensePrice($event){
    console.log("EXPENSE PRICE:--\n", $event.srcElement.value)
    this.countExpenses()
  }



  getAllStudents(){
    // this.selectedStduent = []
    this._studentsService.allStudentsBySchool(this.loggedUser.schoolId)
    .subscribe(students => {
      this.allStudents = students.students
      var key = _.filter(this.allStudents, {id: this.feeslipToEdit.studentId})
      this.selectedStduent = key
      console.log("ALL STUDENTS FROM SCHOOL:--\n", this.allStudents, "\nSTUDENT IN SLIP:---\n" ,this.selectedStduent)
      this.dropdownStudent = {
        singleSelection: true,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    })
  }

  setForm(){
    // var newfeeslip = {
    //   'title': this.feeslipToEdit.title,
    //   'student': this.selectedStduent,
    //   'nationality': this.feeslipToEdit.nationality,
    //   'tax': this.feeslipToEdit.tax,
    //   'submitdate': this.feeslipToEdit.submissionDate,
    //   'recurring': this.feeslipToEdit.isRecurring,
    //   'recurringday': this.feeslipToEdit.recurringDay,
    //   'amount': this.feeslipToEdit.totalAmount,
    // }
    (<FormGroup>this.feeSlipForm).patchValue({'title': this.feeslipToEdit.title,
                                              'student': this.selectedStduent,
                                              'nationality': this.feeslipToEdit.nationality,
                                              'tax': this.feeslipToEdit.tax.replace('%',''),
                                              'submitdate': new Date(this.feeslipToEdit.submissionDate),
                                              'recurring': this.feeslipToEdit.isRecurring,
                                              'recurringday': this.feeslipToEdit.recurringDay,
                                              'amount': this.feeslipToEdit.totalAmount,}, {onlySelf: true})
    this.feeTotal = this.feeslipToEdit.totalAmount
    this.feeTax = parseInt(this.feeslipToEdit.tax.replace('%','')) * 0.01
    this.countExpenses()
  }

  addFeeSlip(values){
    this.expenseFlag = false
    if (this.expenses.length){
      for (let obj of this.expenses){
        if (obj.title === '' || obj.amount === ''){
          this.expenseFlag = true;
          break;
        }
      }
    }else{
      values.expenses = this.expenses
    }
    if (!this.expenseFlag){
      values.schoolId = this.feeslipToEdit.schoolId
      values.feeSlipId = this.feeslipToEdit.id
      values.feeSlipFields = this.expenses
      // values.studentId = values.student[0].id
      // values.submissionDate = values.submitdate.toISOString()
      // values.studentNationality = values.nationality
      // values.isRecurring = values.recurring
      // values.recurringDay = values.recurringdate
      // values.totalAmount = values.amount
      // values.tax = values.tax
      console.log("FEE SLIP FORM VALUES:--\n", values)
      this._feeslipsService.editFeeSlip(values)
      .subscribe(result => {
        console.log("RESULT FROM EDIT FEE SLIP:--\n", result)
        if (result.status){
          // this.feeSlipForm.reset()
          // this.expenses = []
          this.alerts = []
          this.alerts.push({
            type: 'success',
            msg: `FEESLIP UPDATED SUCCESSFULLY!!`,
            timeout: 1200
          });
        }else{
          this.alerts = []
          this.alerts.push({
            type: 'danger',
            msg: `Something Went Wrong!!`,
            timeout: 1000
          });
        }
      })
    }else{
      this.alerts = []
      this.alerts.push({
        type: 'danger',
        msg: `Please Fill Expenses!!`,
        timeout: 1000
      });      
    }
    // this.feeSlipForm.reset()

  }

  goBack(){
    this._router.navigate(['/feeslips'])
  }

}