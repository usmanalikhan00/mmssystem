import {Component, ElementRef} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { SchoolService } from '../../../services/schools.services';
import { TeachersService } from '../../../services/teachers.services';
import * as _ from "lodash";

@Component({
    selector: 'edit-teacher',
    providers: [FormBuilder, SchoolService, TeachersService],
    templateUrl: 'editteacher.html',
    styleUrls: ['editteacher.css']
})

export class EditTeacher {
  
  addTeacherForm: FormGroup;

  toggle: boolean = false
  loggedUser: any = null

  userSchool: any = null
  
  alerts: any = []
  
  bsValue = new Date();

  teacherToEdit: any = null
  teacherId: any = null
  image: any = null


  constructor(private _formBuilder: FormBuilder, 
              private _router: Router,
              private _schoolService: SchoolService,
              private _activatedRouter: ActivatedRoute,
              private _teachersService: TeachersService,
              ) {
    this._buildAddTeacherForm()
  }
  

  private _buildAddTeacherForm(){
    this.addTeacherForm = this._formBuilder.group({
      name: ['', Validators.required],
      passportno: ['', Validators.required],
      joindate: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("TEACHERS COMPONENT STARTED:--\n", this.loggedUser)
    this._activatedRouter.params.subscribe(params => {
      this.teacherId = params['teacherId']; // (+) converts string 'id' to a number
      this._teachersService.getTeacherById(this.teacherId)
      .subscribe(result => {
        this.teacherToEdit = result.teacher
        if (this.teacherToEdit.imageUrl)
          this.image = this.teacherToEdit.imageUrl
        console.log("TEACHER TO EDIT:-----\n", this.teacherToEdit, result)
        var newTeacher = {'name': this.teacherToEdit.name,
                          'passportno': this.teacherToEdit.passportNo,
                          'joindate': new Date(this.teacherToEdit.joiningDate),
                          'username': "none",
                          'password': "none",
                        };
        (<FormGroup>this.addTeacherForm).setValue(newTeacher, {onlySelf:true})
      })
    });
  }


  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      // console.log(myReader.result.toString().replace("data:image/jpeg;base64,", ""));
    }
    myReader.readAsDataURL(file);
  }
  
  submitTeacher(values){
    values.teacherId = this.teacherToEdit.id
    values.isActive = this.teacherToEdit.isActive
    values.joindate = values.joindate.toISOString()
    values.teacherImage = this.image ? this.image.toString().replace("data:image/jpeg;base64,", "") : null
    console.log("*****VALUES FROM EDIT TEACHER FORM:---\n", values)
    this._teachersService.editTeacher(values)
    .subscribe(result => {
      console.log("RESULT FROM ADDING TEACHER:---\n", result)
      if (result.status){
        this.alerts = []
        this.alerts.push({
          type: 'success',
          msg: `Teacher "${result.teacher.name}" Updated Successfully!!`,
          timeout: 7000
        });
      }else{
        this.alerts = []
        this.alerts.push({
          type: 'danger',
          msg: `Something went wrong!!`,
          timeout: 7000
        });
      }
    })
  }


  goBack(){
    console.log("GO BACK FROM TEACHERS")
    this._router.navigate(['/teachers'])
  }

}