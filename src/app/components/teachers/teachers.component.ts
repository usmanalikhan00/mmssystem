import {Component, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { SchoolService } from '../../services/schools.services';
import { TeachersService } from '../../services/teachers.services';
import * as _ from "lodash";

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
    selector: 'teachers',
    providers: [FormBuilder, 
                SchoolService, 
                TeachersService],
    templateUrl: 'teachers.html',
    styleUrls: ['teachers.css']
})

export class Teachers {
  
  addTeacherForm: FormGroup;

  toggle: boolean = false
  loading: boolean = false
  loggedUser: any = null

  allTeachers: any = []
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  constructor(private _formBuilder: FormBuilder, 
              private _router: Router,
              private _schoolService: SchoolService,
              private _teachersService: TeachersService) {
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("TEACHERS COMPONENT STARTED:--\n", this.loggedUser)
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    if (this.loggedUser.role === 'admin')
      this.getAllTeachers()
    else
      this.getAllTeachersBySchoolId(this.loggedUser.schoolId)
  }

  getAllTeachers(){
    this._teachersService.allTeachers()
    .subscribe(teachers => {
      this.allTeachers = teachers.teachers
      console.log("***** ALL TEACHERS *****\n", this.allTeachers,)
      this.dtTrigger.next()
    })
  }

  getAllTeachersBySchoolId(schoolId){
    this._teachersService.allTeachersBySchool(schoolId)
    .subscribe(teachers => {
      this.allTeachers = teachers.teachers
      console.log("***** ALL TEACHERS *****\n", this.allTeachers,)
      this.dtTrigger.next()
    })
  }



  editTeacher(teacher){
    this._router.navigate(['/editteacher', teacher.id])
  }

  showAddForm(){
    this._router.navigate(['/addteacher'])
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  goBack(){
    console.log("GO BACK FROM TEACHERS")
    this._router.navigate(['/dashboard'])
  }

}