import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { SchoolService } from '../../../services/schools.services';
import { TeachersService } from '../../../services/teachers.services';
import * as _ from "lodash";

@Component({
    selector: 'add-teacher',
    providers: [FormBuilder, SchoolService, TeachersService],
    templateUrl: 'addteacher.html',
    styleUrls: ['addteacher.css']
})

export class AddTeacher {
  
  addTeacherForm: FormGroup;

  toggle: boolean = false
  loggedUser: any = null

  userSchool: any = null
  
  alerts: any = []
  image: any = null
  
  bsValue = new Date();

  constructor(private _formBuilder: FormBuilder, 
              private _router: Router,
              private _schoolService: SchoolService,
              private _teachersService: TeachersService,
              ) {
    this._buildAddTeacherForm()
  }
  

  private _buildAddTeacherForm(){
    this.addTeacherForm = this._formBuilder.group({
      name: ['', Validators.required],
      passportno: ['', Validators.required],
      joindate: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("TEACHERS COMPONENT STARTED:--\n", this.loggedUser)
  }


  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      // console.log(myReader.result.toString().replace("data:image/jpeg;base64,", ""));
    }
    myReader.readAsDataURL(file);
  }
  
  submitTeacher(values){
    // if (!this.editFlag){
    values.schoolId = this.loggedUser.schoolId
    values.joindate = values.joindate.toISOString()
    values.teacherImage = this.image ? this.image.toString().replace("data:image/jpeg;base64,", "") : null
    console.log("VALUES FROM TEACHER FORM:---", values)
    this._teachersService.addTeacher(values)
    .subscribe(result => {
      console.log("RESULT FROM ADDING TEACHER:---\n", result)
      this.alerts = []
      this.alerts.push({
        type: 'success',
        msg: `Teacher "${result.teacher.name}" Added Successfully!!`,
        timeout: 7000
      });
      this.addTeacherForm.reset()
      this.image = null
    })
    // }else{
    //   values.teacherId = this.teacherToEdit.id
    //   values.joindate = values.joindate.toISOString()
    //   console.log("*****VALUES FROM EDIT TEACHER FORM:---\n", values)
    //   values.isActive = this.teacherToEdit.isActive
    //   this._teachersService.editTeacher(values)
    //   .subscribe(result => {
    //     console.log("RESULT FROM ADDING TEACHER:---\n", result)
    //     if (result.status){
    //       this.alerts = []
    //       this.alerts.push({
    //         type: 'success',
    //         msg: `Teacher "${result.teacher.name}" Updated Successfully!!`,
    //         timeout: 7000
    //       });
    //       this.teacherToEdit = result.teacher
    //       var newTeacher = {'name': this.teacherToEdit.name,
    //                         'passportno': this.teacherToEdit.passportNo,
    //                         'joindate': new Date(this.teacherToEdit.joiningDate),
    //                         'username': "none",
    //                         'password': "none",
    //                       };
    //       (<FormGroup>this.addTeacherForm).setValue(newTeacher, {onlySelf:true})
    //     }else{
    //       this.alerts = []
    //       this.alerts.push({
    //         type: 'danger',
    //         msg: `Something went wrong!!`,
    //         timeout: 7000
    //       });
    //     }
    //   })
    // }

  }


  // getAllSchools(){
  //   this._schoolService.allSchools()
  //   .subscribe(schools => {
  //     this.allSchools = schools.schools
  //     const key = _.filter(this.allSchools, {userId: this.loggedUser.id})
  //     this.userSchool = key[0]
  //   })
  // }

  // getAllTeachers(){
  //   this._teachersService.allTeachers()
  //   .subscribe(teachers => {
  //     this.allTeachers = teachers.teachers
  //     // console.log("***** ALL TEACHERS *****\n", this.allTeachers,)
  //   })
  // }

  // getAllTeachersBySchoolId(schoolId){
  //   this._teachersService.allTeachersBySchool(schoolId)
  //   .subscribe(teachers => {
  //     this.allTeachers = teachers.teachers
  //     // console.log("***** ALL TEACHERS *****\n", this.allTeachers,)
  //   })
  // }



  // editTeacher(teacher){
  //   this.teacherToEdit = teacher
  //   // console.log("TEACHER TO EDIT:---", this.teacherToEdit)
  //   var newTeacher = {'name': teacher.name,
  //                     'passportno': teacher.passportNo,
  //                     'joindate': new Date(teacher.joiningDate),
  //                     'username': "none",
  //                     'password': "none",
  //                   };
  //   (<FormGroup>this.addTeacherForm).setValue(newTeacher, {onlySelf:true})
  //   this.editFlag = true
  //   this.addFlag = true
  // }

  // showAddForm(){
  //   this.addFlag = true
  // }

  // hideAddForm(){
  //   this.alerts = []
  //   this.addFlag = false
  //   this.editFlag = false
  //   this.addTeacherForm.reset()
  //   if (this.loggedUser.role === 'admin')
  //     this.getAllTeachers()
  //   else
  //     this.getAllTeachersBySchoolId(this.loggedUser.schoolId)
  // }

  reset(){
    // var newTeacher = {'name': this.teacherToEdit.name,
    //                   'passportno': this.teacherToEdit.passportNo,
    //                   'joindate': new Date(this.teacherToEdit.joiningDate),
    //                   'username': "none",
    //                   'password': "none",
    //                 };
    // (<FormGroup>this.addTeacherForm).setValue(newTeacher, {onlySelf:true})
    this.addTeacherForm.reset()
    this.image = null
  }

  goBack(){
    console.log("GO BACK FROM TEACHERS")
    this._router.navigate(['/teachers'])
  }

}