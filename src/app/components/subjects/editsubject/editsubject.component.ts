import {Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { StudentsService } from  '../../../services/students.services';
import { SubjectsService } from  '../../../services/subjects.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
    selector: 'edit-subject',
    providers: [FormBuilder,
                SubjectsService,
                StudentsService],
    templateUrl: 'editsubject.html',
    styleUrls: ['editsubject.css']
})

export class EditSubject {
  
  addSubjectForm: FormGroup;

  loggedUser: any = null
  subjectId: any = null
  subjectToEdit: any = null

  alerts: any = []

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _subjectsService: SubjectsService,
              private _activatedRouter: ActivatedRoute,
              private _router: Router) {
    this._buildAddSubjectForm()
  }
  

  private _buildAddSubjectForm(){
    this.addSubjectForm = this._formBuilder.group({
      title: ['', Validators.required],
    });
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    // console.log("EDIT SUBJECT COMPONENT STARTED:--\n", this.loggedUser)
    this._activatedRouter.params.subscribe(params => {
      this.subjectId = params['subjectId']; // (+) converts string 'id' to a number
      this._subjectsService.getSubjectsById(this.subjectId)
      .subscribe(result => {
        this.subjectToEdit = result.subject
        // console.log("SUBJECt TO EDIT:-----\n", this.subjectToEdit, result)
        var newSubject = {'title': this.subjectToEdit.title };
        (<FormGroup>this.addSubjectForm).setValue(newSubject, {onlySelf:true})
      })
    });
  }

  submitSubject(values){
    values.subjectId = this.subjectToEdit.id
    // console.log("VALUES FROM EDIT SUBJECT FORM:---\n", values)
    this._subjectsService.editSubject(values)
    .subscribe(result => {
      // console.log("RESULT FROM EDIT SUBJECT API:---'\n", result)
      this.alerts = []
      this.alerts.push({
        type: 'success',
        msg: `Subject Updated Successfully!!`,
        timeout: 7000
      });
    })
  }

  goBack(){
    this._router.navigate(['/subjects'])
  }

}