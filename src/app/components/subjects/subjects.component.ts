import {Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from  '@angular/router';
import { StudentsService } from  '../../services/students.services';
import { SubjectsService } from  '../../services/subjects.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
    selector: 'subjects',
    providers: [FormBuilder,
                SubjectsService,
                StudentsService],
    templateUrl: 'subjects.html',
    styleUrls: ['subjects.css']
})

export class Subjects {
  
  loggedUser: any = null

  allSubjects: any = []

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _subjectsService: SubjectsService,
              private _router: Router) {
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    // console.log("SUBJECTS COMPONENT STARTED:--\n", this.loggedUser)
    this.getAllSubjects()
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
  }

  getAllSubjects(){
    this._subjectsService.allSubjectsBySchool(this.loggedUser.schoolId)
    .subscribe(subjects => {
      this.allSubjects = subjects.subjects
      // console.log("***** ALL SUBJECTS *****\n", subjects,)
      this.dtTrigger.next()
    })
    
  }


  showAddForm(){
    this._router.navigate(['/addsubject'])
  }

  editSubject(subject){
    this._router.navigate(['/editsubject', subject.id])
  }

  goBack(){
    this._router.navigate(['/dashboard'])
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}