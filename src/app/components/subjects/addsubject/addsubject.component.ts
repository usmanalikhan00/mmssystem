import {Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from  '@angular/router';
import { StudentsService } from  '../../../services/students.services';
import { SubjectsService } from  '../../../services/subjects.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
    selector: 'add-subject',
    providers: [FormBuilder,
                SubjectsService,
                StudentsService],
    templateUrl: 'addsubject.html',
    styleUrls: ['addsubject.css']
})

export class AddSubject {
  
  addSubjectForm: FormGroup;

  loggedUser: any = null

  alerts: any = []

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _subjectsService: SubjectsService,
              private _router: Router) {
    this._buildAddSubjectForm()
  }
  

  private _buildAddSubjectForm(){
    this.addSubjectForm = this._formBuilder.group({
      title: ['', Validators.required],
    });
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    // console.log("ADD SUBJECT COMPONENT STARTED:--\n", this.loggedUser)
  }

  submitSubject(values){
    values.schoolId = this.loggedUser.schoolId
    // console.log("VALUES FROM SUBJECT FORM:---\n", values)
    this._subjectsService.addSubject(values)
    .subscribe(result => {
      // console.log("RESULT FROM ADD SUBJECT API:---'\n", result)
      this.alerts = []
      this.alerts.push({
        type: 'success',
        msg: `Subject "${result.subject.title}" Added Successfully!!`,
        timeout: 7000
      });
      this.addSubjectForm.reset()
    })
  }


  // editSubject(subject){
  //   this.subjectToEdit = subject
  //   console.log("VALUES FROM EDIT SUBJECT:---\n", this.subjectToEdit)
  //   var newSubject = {'title': subject.title };
  //   (<FormGroup>this.addSubjectForm).setValue(newSubject, {onlySelf:true})
  //   this.editFlag = true
  //   this.addFlag = true
  // }

  reset(){
    this.addSubjectForm.reset()
  }

  goBack(){
    this._router.navigate(['/subjects'])
  }

}