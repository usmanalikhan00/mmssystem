import {Component, ElementRef, Input} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { InboxService } from  '../../services/inbox.service';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
    selector: 'single-chat',
    providers: [FormBuilder, InboxService],
    templateUrl: 'singlechat.html',
    styleUrls: ['singlechat.css']
})

export class SingleChat {
  
  constructor(private _formBuilder: FormBuilder, 
              private _inboxService: InboxService,
              private _activatedRouter: ActivatedRoute,
              private _router: Router) {
  }

  toggle: boolean = false
  loggedUser: any = null
  allChats: any = []
  chatId: any = []

  @Input('singleChat') singleChat: any = null

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    this._activatedRouter.params.subscribe(params => {
      this.chatId = params['chatId']; // (+) converts string 'id' to a number
      console.log("CHAT TO GET:---\n", this.chatId)
    });
  }

  getSenderInfo(parentId){
    var parent = null
    // return parent;
  }

  goBack(){
    this._router.navigate(['/dashboard'])
  }

  ngAfterViewInit(){
  }

}