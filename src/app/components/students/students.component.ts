import {Component, ElementRef, ViewChild} from '@angular/core';
import { Router } from  '@angular/router';
import { StudentsService } from  '../../services/students.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';


@Component({
    selector: 'students',
    providers: [FormBuilder,
                StudentsService],
    templateUrl: 'students.html',
    styleUrls: ['students.css']
})

export class Students {
  

  toggle: boolean = false
  loggedUser: any = null
  loading: boolean = false


  allStudents: any = []

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _router: Router) {
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("STUDENTS COMPONENT STARTED:--\n", this.loggedUser)
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    if (this.loggedUser.role === 'admin')
      this.getAllStudents()
    else
      this.getAllStudentsBySchoolId(this.loggedUser.schoolId)

  }

  getAllStudents(){
    this._studentsService.allStudents()
    .subscribe(students => {
      this.allStudents = students.students
      console.log("***** ALL STUENTS *****\n", this.allStudents,)
      this.dtTrigger.next()
    })
  }

  getAllStudentsBySchoolId(schoolId){
    this._studentsService.allStudentsBySchool(schoolId)
    .subscribe(students => {
      this.allStudents = students.students
      console.log("***** ALL STUENTS *****\n", this.allStudents,)
      this.dtTrigger.next()
    })
  }

  editStudent(student){
    this._router.navigate(['/editstudent', student.id])
  }

  showAddForm(){
    this._router.navigate(['/addstudent'])
  }

  goBack(){
    this._router.navigate(['/dashboard'])
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}