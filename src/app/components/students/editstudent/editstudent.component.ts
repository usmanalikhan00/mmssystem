import {Component, ElementRef} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { StudentsService } from  '../../../services/students.services';
import { RouterService } from '../../../services/routing.service';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as _ from 'lodash'

@Component({
    selector: 'edit-student',
    providers: [FormBuilder,
                StudentsService,
                RouterService],
    templateUrl: 'editstudent.html',
    styleUrls: ['editstudent.css']
})

export class EditStudent {
  
  addStudentForm: FormGroup;

  toggle: boolean = false
  loggedUser: any = null

  studentToEdit: any = null
  studentId: any = null
  image: any = null

  alerts: any = []

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _RouterService: RouterService,
              private _activatedRouter: ActivatedRoute,
              private _router: Router) {
    this._buildAddStduentForm()
  }
  

  private _buildAddStduentForm(){
    this.addStudentForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      passportno: ['', Validators.required],
      nationality: ['', Validators.required],
      joindate: ['', Validators.required],
      schoolregid: ['', Validators.required],
      dob: ['', Validators.required],
      age: ['', Validators.required],
      pname: ['', Validators.required],
      paddress: ['', Validators.required],
      ppassportno: ['', Validators.required],
      pnationality: ['', Validators.required],
      prelation: ['', Validators.required],
      pphone: ['', Validators.required],
      pmobile: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    
    this._activatedRouter.params.subscribe(params => {
      this.studentId = params['studentId']; // (+) converts string 'id' to a number
      this._studentsService.allStudents()
      .subscribe(result => {
        // this.studentToEdit = result.student
        var tempStudnets = result.students
        // console.log("ADD STUDENT COMPONENT STARTE  :--\n", this.loggedUser, this.studentId, tempStudnets)
        var key = _.filter(tempStudnets, {"id": parseInt(this.studentId)})
        this.studentToEdit = key[0]
        console.log("STUDENT TO EDIT:-----\n", this.studentToEdit, result, key)
        this.image = this.studentToEdit.imageUrl
        var newStudent = {'name': this.studentToEdit.name,
                          'passportno': this.studentToEdit.passportNo,
                          'joindate': new Date(this.studentToEdit.registrationDate),
                          'dob': new Date(this.studentToEdit.dateOfBirth),
                          'schoolregid': this.studentToEdit.schoolIdentityNo,
                          'address': this.studentToEdit.address,
                          'nationality': this.studentToEdit.nationality,
                          'age': this.studentToEdit.age,
                          'pname': this.studentToEdit.studentParent.name,
                          'paddress': this.studentToEdit.studentParent.address,
                          'pmobile': this.studentToEdit.studentParent.mobile,
                          'pphone': this.studentToEdit.studentParent.phone,
                          'prelation': this.studentToEdit.studentParent.relationWithStudent,
                          'ppassportno': this.studentToEdit.studentParent.passportNo,
                          'pnationality': this.studentToEdit.studentParent.nationality,
                          'username': "none",
                          'password': "none",
                        };
        (<FormGroup>this.addStudentForm).setValue(newStudent, {onlySelf:true})
      })
    });
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      // console.log(myReader.result.toString().replace("data:image/jpeg;base64,", ""));
    }
    myReader.readAsDataURL(file);
  }

  submitStudent(values){
    values.studentId = this.studentToEdit.id
    values.studentIsActive = this.studentToEdit.isActive
    values.parentId = this.studentToEdit.studentParent.id
    values.parentIsActive = this.studentToEdit.studentParent.isActive
    values.joindate = values.joindate.toISOString() 
    values.dob = values.dob.toISOString() 
    console.log("VALUES FROM EDIT STUDENT FORM:---\n", values)
    this._studentsService.editStudent(values)
    .subscribe(result => {
      console.log("RESULT FROM EDIT STUDENT API:---'\n", result)
      this.alerts = []
      this.alerts.push({
        type: 'success',
        msg: `Stduent "${result.student.name}" Updated Successfully!!`,
        timeout: 7000
      });
    })
  }


  // editStudent(student){
  //   this.studentToEdit = student
  //   console.log("VALUES FROM EDIT STUDENT:---\n", student)
  //   var newStudent = {'name': student.name,
  //                     'passportno': student.passportNo,
  //                     'joindate': new Date(student.registrationDate),
  //                     'dob': new Date(student.dateOfBirth),
  //                     'schoolregid': student.schoolIdentityNo,
  //                     'address': student.address,
  //                     'nationality': student.nationality,
  //                     'age': student.age,
  //                     'pname': student.studentParent.name,
  //                     'paddress': student.studentParent.address,
  //                     'pmobile': student.studentParent.mobile,
  //                     'pphone': student.studentParent.phone,
  //                     'prelation': student.studentParent.relationWithStudent,
  //                     'ppassportno': student.studentParent.passportNo,
  //                     'pnationality': student.studentParent.nationality,
  //                     'username': "none",
  //                     'password': "none",
  //                   };
  //   (<FormGroup>this.addStudentForm).setValue(newStudent, {onlySelf:true})
  //   this.editFlag = true
  //   this.addFlag = true
  // }

  // showAddForm(){
  //   this.addFlag = true
  // }

  // hideAddForm(){
  //   this.addFlag = false
  //   this.editFlag = false
  //   this.addStudentForm.reset()
  //   this.getAllStudents()
  //   this.alerts = []
  // }

  reset(){
    this.addStudentForm.reset()
    this.image = null
  }

  goBack(){
    this._router.navigate(['/students'])
  }

}