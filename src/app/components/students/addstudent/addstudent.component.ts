import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { StudentsService } from  '../../../services/students.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'add-student',
    providers: [FormBuilder,
                StudentsService],
    templateUrl: 'addstudent.html',
    styleUrls: ['addstudent.css']
})

export class AddStudent {
  
  addStudentForm: FormGroup;

  toggle: boolean = false
  loggedUser: any = null

  alerts: any = []
  image: any = null

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _router: Router) {
    this._buildAddStduentForm()
  }
  

  private _buildAddStduentForm(){
    this.addStudentForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      passportno: ['', Validators.required],
      nationality: ['', Validators.required],
      joindate: ['', Validators.required],
      schoolregid: ['', Validators.required],
      dob: ['', Validators.required],
      age: ['', Validators.required],
      pname: ['', Validators.required],
      paddress: ['', Validators.required],
      ppassportno: ['', Validators.required],
      pnationality: ['', Validators.required],
      prelation: ['', Validators.required],
      pphone: ['', Validators.required],
      pmobile: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("ADD STUDENT COMPONENT STARTED:--\n", this.loggedUser)
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      // console.log(myReader.result.toString().replace("data:image/jpeg;base64,", ""));
    }
    myReader.readAsDataURL(file);
  }

  submitStudent(values){
    console.log("VALUES FROM STUDENT FORM:---\n", values)
    values.schoolId = this.loggedUser.schoolId
    values.joindate = values.joindate.toISOString()
    values.dob = values.dob.toISOString()
    values.studentImage = this.image ? this.image.toString().replace("data:image/jpeg;base64,", "") : null
    this._studentsService.addStudent(values)
    .subscribe(result => {
      console.log("RESULT FROM ADD STUDENT API:---'\n", result)
      this.alerts = []
      this.alerts.push({
        type: 'success',
        msg: `Stduent "${result.student.name}" Added Successfully!!`,
        timeout: 7000
      });
      this.addStudentForm.reset()
      this.image = null
    })
  }

  reset(){
    this.addStudentForm.reset()
    this.image = null
  }
  
  goBack(){
    this._router.navigate(['/students'])
  }

}