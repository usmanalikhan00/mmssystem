import {Component, ElementRef, Output, EventEmitter} from '@angular/core';
import { Router } from  '@angular/router';
import { InboxService } from  '../../services/inbox.service';
import { StudentsService } from  '../../services/students.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
    selector: 'app-inbox',
    providers: [FormBuilder, InboxService, StudentsService],
    templateUrl: 'inbox.html',
    styleUrls: ['inbox.css']
})

export class Inbox {
  
  constructor(private _formBuilder: FormBuilder, 
              private _inboxService: InboxService,
              private _studentsService: StudentsService,
              private _router: Router) {
  }

  toggle: boolean = false
  showChat: boolean = false
  selectedChat: any = null
  selectedChatIndex: any = null
  loggedUser: any = null
  allStudents: any = null
  selectedStudent: any = null
  dropdownStudent: any = null
  receiver: any = null
  message: any = null
  allChats: any = []
  alerts: any = []
  @Output() singleChat = new EventEmitter<any>(); 
  sendMessage: boolean = false
  inlineChatMsg: any = null


  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    this.getAllChats()
  }

  getAllChats(){
    this._inboxService.allChatsByUserId(this.loggedUser.school.userId)
    .subscribe(result => {
      this.allChats = result.conversations
      for (let chat of this.allChats){
        chat.date = moment.utc(chat.messages[0].dateCreated).local().fromNow()
        if (this.loggedUser.school.userId === chat.messages[0].senderId){
          this._inboxService.parentByUserId(chat.messages[0].receiverId)
          .subscribe(result => {
            // console.log("USER NAME TO SHOW IN CHAT:---\n", result)
            chat.parent = result.student  
          })
        }
        if (this.loggedUser.school.userId === chat.messages[0].receiverId){
          this._inboxService.parentByUserId(chat.messages[0].senderId)
          .subscribe(result => {
            chat.parent = result.student  
          })
        }
      }
      console.log("ALL CHATS FOR SCHOOL:---\n", this.allChats)
    })
  }

  addChat(){
    this.sendMessage = true
    this._studentsService.allStudentsBySchool(this.loggedUser.school.id)
    .subscribe(result => {
      console.log("ALL STUDENTS FOR THE SCHOOL:--", result.students)
      this.allStudents = result.students
      this.dropdownStudent = {
        singleSelection: true,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    })
  }

  addNewChat(){
    var key = _.filter(this.allStudents, {'id': this.selectedStudent[0].id})
    this.receiver = key[0]
    var chat = {
      'senderId': this.loggedUser.id,
      'receiverId': this.receiver.studentParent.userId,
      'message': this.message
    }
    console.log("CHAT TO SAVE:--\n", chat)
    this._inboxService.addNewChat(chat)
    .subscribe(result => {
      console.log("RESULT AFETR ADDING THE CHAT0:--\n", result)
      this.selectedStudent = null
      this.message = null
      this.alerts.push({
        type: 'success',
        msg: `Message Sent Successfully!!`,
        timeout: 1500
      });
    })
    
  }


  sendInlineMsg(){
    var receiverId = null
    if (this.loggedUser.id === this.selectedChat.messages[0].senderId){
      receiverId = this.selectedChat.messages[0].receiverId
    }else{
      receiverId = this.selectedChat.messages[0].senderId
    }
    var chat = {
      'senderId': this.loggedUser.id,
      'receiverId': receiverId,
      'message': this.inlineChatMsg
    }
    console.log("SELECTED CHAT IS:---\n", this.selectedChat, 
                "\nCHAT TO SAVE IS:---\n", chat,)
    this._inboxService.addNewChat(chat)
    .subscribe(result => {
      // console.log("RESULT AFETR ADDING THE CHAT0:--\n", result)
      // this.selectedStudent = null
      // this.message = null
      this.inlineChatMsg = null
      this.alerts.push({
        type: 'success',
        msg: `Message Sent Successfully!!`,
        timeout: 1500
      });
      this.getAllChats()
    })

  }

  goToChat(chat, i){
    console.log("CHAT TO GET:---\n", chat, "\nSELCTED CHAT INDEX:---\n", i,);
    this.selectedChat = chat;
    this.selectedChatIndex = i
    this.inlineChatMsg = null
    for (let chat of this.selectedChat.messages){
      chat.date = moment.utc(chat.dateCreated).local().fromNow()
      if (this.loggedUser.id === chat.senderId)
        chat.sendflag = 'sender'
      if (this.loggedUser.id === chat.receiverId)
        chat.sendflag = 'receiver'
    }
    // this.showChat = true;
    // this.singleChat.emit(chat);
    // this._router.navigate(['/singleinbox', {'chatId': JSON.stringify(chat)}])
  }

  hideChat(){
    this.showChat = false;
    this.getAllChats()
  }

  hideSendMessage(){
    this.sendMessage = false
    this.selectedStudent = null
    this.message = null
    this.getAllChats()
  }

  goBack(){
    this._router.navigate(['/dashboard'])
  }

  ngAfterViewInit(){
  }

}