import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

import { TeachersService } from '../../services/teachers.services'
import { SchoolService } from '../../services/schools.services'
import { StudentsService } from '../../services/students.services'
import { ClassesService } from '../../services/classes.services'
import { SubjectsService } from '../../services/subjects.services'
import { FeeslipsService } from '../../services/feeslips.services'

@Component({
    selector: 'dashboard',
    providers: [FormBuilder,
                SchoolService,
                StudentsService,
                ClassesService,
                SubjectsService,
                FeeslipsService,
                TeachersService],
    templateUrl: 'dashboard.html',
    styleUrls: ['dashboard.css']
})

export class Dashboard {
  
  loggedUser: any = null
  schoolsCount: any = null
  teachersCount: any = null
  studentsCount: any = null
  parentsCount: any = null
  subjectsCount: any = null
  classesCount: any = null
  feeslipsCount: any = null



  constructor(private _formBuilder: FormBuilder, 
              private _router: Router,
              private _schoolService: SchoolService,
              private _studentsService: StudentsService,
              private _classesService: ClassesService,
              private _subjectsService: SubjectsService,
              private _feeslipsService: FeeslipsService,
              private _teachersService: TeachersService) {

  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("DASHBOARD STARTED:--", this.loggedUser)
    if (this.loggedUser.role === 'admin'){
      this._schoolService.allSchoolsCount()
      .subscribe(count => {
        this.schoolsCount = count.schoolsCount
        console.log("ALL SCHOOLS COUNT:--\n", this.schoolsCount)
        this._teachersService.allTeachersCount()
        .subscribe(count => {
          this.teachersCount = count.teachersCount
          console.log("ALL TEACHERS CIOYNT:--\n", this.teachersCount)
          this._studentsService.allStudentsCount()
          .subscribe(count => {
            this.studentsCount = count.studentsCount
            console.log("ALL STUDENTS COUNT:--\n", this.studentsCount)
          })
        })
      })
    }
    if (this.loggedUser.role === 'school'){
      this._teachersService.allTeachersCountBySchool(this.loggedUser.schoolId)
      .subscribe(count => {
        this.teachersCount = count.teachersCount
        console.log("ALL TEACHERS CIOYNT:--\n", this.teachersCount)
        this._studentsService.allStudentsCountBySchool(this.loggedUser.schoolId)
        .subscribe(count => {
          this.studentsCount = count.studentsCount
          console.log("ALL STUDENTS COUNT:--\n", this.studentsCount)
          this._classesService.allClassesCountBySchool(this.loggedUser.schoolId)
          .subscribe(count => {
            this.classesCount = count.classesCount
            console.log("ALL STUDENTS COUNT:--\n", this.studentsCount)
            this._subjectsService.allSubjectsCountBySchool(this.loggedUser.schoolId)
            .subscribe(count => {
              this.subjectsCount = count.subjectsCount
              console.log("ALL STUDENTS COUNT:--\n", this.studentsCount)
              this._feeslipsService.allFeeSlipsCountBySchool(this.loggedUser.schoolId)
              .subscribe(count => {
                this.feeslipsCount = count.feeSlipsCount
                console.log("ALL FEE SLIPS COUNT:--\n", this.feeslipsCount)
              })
            })
          })
        })
      })
    }

  }

  goToPage(route){
    this._router.navigate([route])
  }

  mouseEnter($event){
    console.log("mouse enter : " + $event);
  }

  mouseLeave(div : string){
    console.log('mouse leave :' + div);
  }


}