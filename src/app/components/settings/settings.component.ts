import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { SchoolService } from  '../../services/schools.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'settings',
    providers: [FormBuilder, 
                SchoolService],
    templateUrl: 'settings.html',
    styleUrls: ['settings.css']
})

export class Settings {
  
  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService,
              private _router: Router) {
  }

  toggle: boolean = false
  loggedUser: any = null
  selectedSchool: any = null
  image: any = null



  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
  }

  goToSchoolEdit(){
    this._router.navigate(['/editschool', this.selectedSchool.id])
  }

  goBack(){
    this._router.navigate(['/dashboard'])
  }

  ngAfterViewInit(){
    this._schoolService.getSchoolById(this.loggedUser.schoolId)
    .subscribe(school => {
      this.selectedSchool = school.school
      this.image = this.selectedSchool.imageUrl
      console.log("SELCTED SCHOOL:--\n", this.selectedSchool)
    })
    
  }

}