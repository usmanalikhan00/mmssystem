import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { SchoolService } from  '../../../services/schools.services';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'change-password',
    providers: [FormBuilder, 
                SchoolService],
    templateUrl: 'changepassword.html',
    styleUrls: ['changepassword.css']
})

export class ChangePassword {
  
  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService,
              private _router: Router) {
  }

  toggle: boolean = false
  loggedUser: any = null

  password: any = null
  repassword: any = null
  passwordmatch: boolean = false



  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
  }

  passChanges($event){
    console.log("CVALUES OF PASSSWORD:--\n", $event.srcElement.value, this.password)
    if (this.password === this.repassword)
      this.passwordmatch = true
  }

  repassChanges($event){
    console.log("CVALUES OF REE PASSSWORD:--\n", $event.srcElement.value, this.password)
    if (this.password === this.repassword)
      this.passwordmatch = true
  
  }

  setPassword(){
    this._schoolService.resetPassword(this.loggedUser.id, this.password)
    .subscribe(result => {
      console.log("PASSWORD CHANGED:--", result)
      this._router.navigate(['/dashboard'])
    })
  }
}