// import {Component, ElementRef} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as _ from 'lodash';
import * as moment from 'moment'
import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';

import { ClassesService } from '../../../services/classes.services'
import { PlannerService } from '../../../services/planner.services'

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};


@Component({
    selector: 'edit-planner',
    providers: [FormBuilder,
                ClassesService,
                PlannerService,
               ],
    templateUrl: 'editplanner.html',
    styleUrls: ['editplanner.css']
})

export class EditPlanner {

  activeDayIsOpen: boolean = true;
  showPlannerForm: boolean = false;
  editPlannerForm: boolean = false;

  addPlannerForm: FormGroup;
  loggedUser: any = null
  allPlanners: any = []
  allClasses: any = []

  dropdownClasses: any = null
  selectedClasses: any = []

  alerts: any = null
  selectedEvent: any = null
  plannerId: any = null
  
  constructor(private _formBuilder: FormBuilder,
              private _classesService: ClassesService, 
              private _plannerService: PlannerService, 
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this._buildPlannerForm()
  }

  _buildPlannerForm(){
    this.addPlannerForm = this._formBuilder.group({
      title: ['', Validators.required],
      class: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      type: ['', Validators.required],
      // classtype: ['single',],
    })
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    this._activatedRouter.params.subscribe(params => {
      this.plannerId = params['plannerId'];
      this._plannerService.getPlannerById(this.plannerId)
      .subscribe(planner => {
        this.selectedEvent = planner.planner
        console.log("EDIT PLANNER STARTED:---\n", this.loggedUser, planner, this.plannerId,
                    "EDIT PLANNER STARTED:---\n", this.selectedEvent,)
        this.getAllClasses();
        (<FormGroup>this.addPlannerForm).setValue({ 'title': this.selectedEvent.title, 
                                                    'class': this.selectedClasses, 
                                                    'startDate': new Date(this.selectedEvent.startDate), 
                                                    'type': this.selectedEvent.type.toString(), 
                                                    'endDate': new Date(this.selectedEvent.endDate) }, {onlySelf: true})
      })
    });
  }

  getAllClasses(){
    this._classesService.allClassesBySchool(this.loggedUser.schoolId)
    .subscribe(classes => {
      console.log("***** ALL CLASSES *****\n", classes)
      this.allClasses = classes.classes
      var all = {id: 0, name: 'All Classes'}
      this.allClasses.unshift(all)
      if (this.selectedEvent.classes.length > 1){
        this.selectedClasses = [this.allClasses[0]]
      }else{
        this.selectedClasses = this.selectedEvent.classes
      }
      this.dropdownClasses = {
        singleSelection: true,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    })

  }


  hideEditPlannerForm(){
    this._router.navigate(['/planner'])
  }

  updatePlanner(values){
    // values.schoolId = this.loggedUser.schoolId
    values.plannerId = this.selectedEvent.id
    values.classId = values.class[0].id
    values.startDate = moment(values.startDate).utcOffset(0, true).format()
    values.endDate = moment(values.endDate).utcOffset(0, true).format()
    console.log("VALUES FROM EDIT PLANNER:---\n", values)
    this._plannerService.editPlanner(values)
    .subscribe(result => {
      if (result.status){
        this.alerts = []
        this.alerts.push({
          type: 'info',
          msg: `Event Updated Successfully!!`,
          timeout: 4000
        });
        // this.addPlannerForm.reset()
      }else{
        this.alerts = []
        this.alerts.push({
          type: 'danger',
          msg: `Something Went Wrong!!`,
          timeout: 4000
        });


      }
      
    })
  }



}