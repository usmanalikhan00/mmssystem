// import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as _ from 'lodash';
import * as moment from 'moment';
import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarEventTitleFormatter 
} from 'angular-calendar';
import { CustomEventTitleFormatter } from './event-title-formatter.provider';
import { ClassesService } from '../../services/classes.services'
import { PlannerService } from '../../services/planner.services'

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'planner',
    providers: [FormBuilder,
                ClassesService,
                PlannerService,
                {
                  provide: CalendarEventTitleFormatter,
                  useClass: CustomEventTitleFormatter
                }
               ],
    templateUrl: 'planner.html',
    styleUrls: ['planner.css']
})

export class Planner {


  modalRef: BsModalRef;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    // {
    //   start: subDays(startOfDay(new Date()), 1),
    //   end: addDays(new Date(), 1),
    //   title: 'A 3 day event',
    //   color: colors.red,
    //   actions: this.actions,
    //   allDay: true,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true
    //   },
    //   draggable: true
    // },
    // {
    //   start: startOfDay(new Date()),
    //   title: 'An event with no end date',
    //   color: colors.yellow,
    //   actions: this.actions
    // },
    // {
    //   start: subDays(endOfMonth(new Date()), 3),
    //   end: addDays(endOfMonth(new Date()), 3),
    //   title: 'A long event that spans 2 months',
    //   color: colors.blue,
    //   allDay: true
    // },
    // {
    //   start: addHours(startOfDay(new Date()), 2),
    //   end: new Date(),
    //   title: 'A draggable and resizable event',
    //   color: colors.yellow,
    //   actions: this.actions,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true
    //   },
    //   draggable: true
    // }
  ];

  activeDayIsOpen: boolean = true;
  showPlannerForm: boolean = false;
  editPlannerForm: boolean = false;

  addPlannerForm: FormGroup;
  loggedUser: any = null
  allPlanners: any = []
  allClasses: any = []

  dropdownClasses: any = null
  selectedClasses: any = []

  alerts: any = null
  selectedEvent: any = null
  eventtype: any = ''

  @ViewChild('template')
  template: TemplateRef<any>;
  
  constructor(private _formBuilder: FormBuilder,
              private _classesService: ClassesService, 
              private _modalService: BsModalService, 
              private _plannerService: PlannerService, 
              private _router: Router) {
    this._buildPlannerForm()
  }

  _buildPlannerForm(){
    this.addPlannerForm = this._formBuilder.group({
      title: ['', Validators.required],
      class: ['', Validators.required],
      startDate: [new Date(), Validators.required],
      endDate: [new Date(), Validators.required],
      type: ['', Validators.required],
      // classtype: ['single',],
    })
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("PLANNER STARTED:---\n", this.loggedUser)
    this.getAllClasses();
    this.getAllPlanners();
  }

  getAllPlanners(){
    this._plannerService.getPlannersBySchoolId(this.loggedUser.schoolId)
    .subscribe(planners => {
      this.events = []
      var object = null
      this.allPlanners = planners.planners
      for (let obj of this.allPlanners){
        if (obj.type === parseInt("1")){
          object = {
            plannerId: obj.id,
            start: new Date(obj.startDate),
            end: new Date(obj.endDate),
            title: obj.title,
            color: colors.red,
            actions: this.actions,
            // allDay: true,
            // resizable: {
            //   beforeStart: true,
            //   afterEnd: true
            // },
            draggable: true
          }
        }else{
          object = {
            plannerId: obj.id,
            start: new Date(obj.startDate),
            end: new Date(obj.endDate),
            title: obj.title,
            color: colors.blue,
            actions: this.actions,
            // allDay: true,
            // resizable: {
            //   beforeStart: true,
            //   afterEnd: true
            // },
            draggable: true
          }
        }
        this.events.push(object)
      }
      console.log("ALL PLANNER:---\n", this.allPlanners)
      this.refresh.next()
    })
  }

  getAllClasses(){
    this._classesService.allClassesBySchool(this.loggedUser.schoolId)
    .subscribe(classes => {
      console.log("***** ALL CLASSES *****\n", classes)
      this.allClasses = classes.classes
      var all = {id:'0', name:'All Classes'}
      this.allClasses.unshift(all)
      this.dropdownClasses = {
        singleSelection: true,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        // unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
      // for (let obj of this.allClasses){
      //   obj.students ? obj.studentsCount = obj.students.length : obj.studentsCount = 0
      // }
    })

  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event): void {
    this.modalData = { event, action };
    // this.modal.open(this.modalContent, { size: 'lg' });
    var key = _.filter(this.allPlanners, {"id": event.plannerId})
    console.log("MODEL DATA EVENT CHNAGED:--\n", this.modalData, "\nKEY FILTER DATA:--\n", key[0])
    this.selectedEvent = key[0]
    if (action === 'Edited'){
      // this.editPlannerForm = true
      this.selectedClasses = this.selectedEvent.classes
      // this.setForm()
      this._router.navigate(['/editplanner', this.selectedEvent.id])
    }
    if (action === 'Clicked'){
      // this.editPlannerForm = true
      // this.selectedClasses = this.selectedEvent.classes
      // this.setForm()
      // this._router.navigate(['/editplanner', this.selectedEvent.id])
      // this.editPlannerForm = true
      this.openModal(this.template)
    }

  }
  
  openModal(template: TemplateRef<any>) {
    this.modalRef = this._modalService.show(template);
  }

  setForm(){
    (<FormGroup>this.addPlannerForm).setValue({ 'title': this.selectedEvent.title, 
                                                'class': this.selectedClasses, 
                                                'startDate': new Date(this.selectedEvent.startDate), 
                                                'type': this.selectedEvent.type.toString(), 
                                                'endDate': new Date(this.selectedEvent.endDate) }, {onlySelf: true})

  }

  eventTypeChanged($event){
    this.events = []
    var object = null
    if (this.eventtype){

      if (this.eventtype === 'timetable'){
        const key = _.filter(this.allPlanners, {"type": 1})
        console.log("EVENT TYPE FROM DROPDOWN:---\n", $event, "\n", this.eventtype, "\n", key)
        for (let obj of key){
          // if (obj.type === parseInt("1")){
          object = {
            plannerId: obj.id,
            start: new Date(obj.startDate),
            end: new Date(obj.endDate),
            title: obj.title,
            color: colors.red,
            actions: this.actions,
            // allDay: true,
            // resizable: {
            //   beforeStart: true,
            //   afterEnd: true
            // },
            draggable: true
          }
          // }
          this.events.push(object)
        }
      }else if (this.eventtype === 'event'){
        const key = _.filter(this.allPlanners, {"type": 2})
        console.log("EVENT TYPE FROM DROPDOWN:---\n", $event, "\n", this.eventtype, "\n", key)
        for (let obj of key){
          // if (obj.type === parseInt("1")){
          object = {
            plannerId: obj.id,
            start: new Date(obj.startDate),
            end: new Date(obj.endDate),
            title: obj.title,
            color: colors.blue,
            actions: this.actions,
            // allDay: true,
            // resizable: {
            //   beforeStart: true,
            //   afterEnd: true
            // },
            draggable: true
          }
          // }
          this.events.push(object)
        }
      }
      this.refresh.next()
    }else{
      this.getAllPlanners()
    }
  }

  hideEditPlannerForm(){
    this.editPlannerForm = false
    // this.addPlannerForm.reset()
    // this.getAllPlanners()
  }

  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }

  addNewEvent(): void {
    this.showPlannerForm = true
  }

  goBack(){
    this.showPlannerForm = false
    this.addPlannerForm.reset()
    this.getAllPlanners()
    this.refresh.next();
    // this._router.navigate(['/planner'])
  }

  submitPlanner(values){
    values.schoolId = this.loggedUser.schoolId
    values.classId = values.class[0].id
    values.startDate = moment(values.startDate).utcOffset(0, true).format()
    values.endDate = moment(values.endDate).utcOffset(0, true).format()
    console.log("VALUES FROM PLANNER TABLE:---\n", values)
    this._plannerService.addPlanner(values)
    .subscribe(result => {
      if (result.status){
        this.alerts = []
        this.alerts.push({
          type: 'info',
          msg: `Event Added Successfully!!`,
          timeout: 4000
        });
        this.addPlannerForm.reset()
      }else{
        this.alerts = []
        this.alerts.push({
          type: 'danger',
          msg: `Something went Wrong!!`,
          timeout: 4000
        });
      }
    })
  }


}