import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'parents',
    providers: [FormBuilder],
    templateUrl: 'parents.html',
    styleUrls: ['parents.css']
})

export class Parents {
  
  // addTeacherForm: FormGroup;

  // toggle: boolean = false
  loggedUser: any = null

  addFlag: boolean = false
  editFlag: boolean = false

  allParents: any = []

  constructor(private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    console.log("PARENTS COMPONENT STARTED")
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
  }

  // submitParent(values){
  //   console.log("VALUES FROM TEACHER FORM:---", values)
  // }


  // editParent(teacher){
  //   console.log("VALUES FROM EDIT TEACHER:---", teacher)
    
  // }

  // showAddForm(){
  //   this.addFlag = true
  // }

  // hideAddForm(){
  //   this.addFlag = false
  //   this.editFlag = false
  //   this.addTeacherForm.reset()
  //   // this.getAllSchools()
  // }


  goBack(){
    this._router.navigate(['/dashboard'])
  }

}