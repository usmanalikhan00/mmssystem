import {Component, ElementRef, ViewChild} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { StudentsService } from  '../../services/students.services';
import { SubjectsService } from  '../../services/subjects.services';
import { TeachersService } from  '../../services/teachers.services';
import { ClassesService } from  '../../services/classes.services';

import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';


@Component({
    selector: 'classes',
    providers: [FormBuilder,
                SubjectsService,
                TeachersService,
                ClassesService,
                StudentsService],
    templateUrl: 'classes.html',
    styleUrls: ['classes.css']
})

export class Classes {
  
  loggedUser: any = null

  allClasses: any = []
  
  classToEdit: any = null

  title = 'app';

  columnDefs = [
      {headerName: 'Name', field: 'name' },
      {headerName: 'Date/Time', field: 'dateCreated' },
      {headerName: 'Students', field: 'studentsCount'}
  ];

  rowData = [
      { make: 'Toyota', model: 'Celica', price: 35000 },
      { make: 'Ford', model: 'Mondeo', price: 32000 },
      { make: 'Porsche', model: 'Boxter', price: 72000 }
  ];
  
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;


  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _subjectsService: SubjectsService,
              private _teachersService: TeachersService,
              private _classesService: ClassesService,
              private _router: Router) {

    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("CLASSES COMPONENT STARTED:--\n", this.loggedUser)
  
  }

  ngOnInit(){
    this.getAllClasses()
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

  }



  getAllClasses(){
    this._classesService.allClassesBySchool(this.loggedUser.schoolId)
    .subscribe(classes => {
      console.log("***** ALL CLASSES *****\n", classes)
      this.allClasses = classes.classes
      for (let obj of this.allClasses){
        obj.students ? obj.studentsCount = obj.students.length : obj.studentsCount = 0
      }
      this.dtTrigger.next();
      this.rowData = this.allClasses
    })
  }


  editClass(obj){
    this.classToEdit = obj
    this._router.navigate(['/editclass', this.classToEdit.id])
  }

  showAddForm(){
    this._router.navigate(['/addclass'])
  }


  goBack(){
    this._router.navigate(['/dashboard'])
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  // ngAfterViewInit(): void {
  //   this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //     dtInstance.columns().every(function () {
  //       const that = this;
  //       $('input', this.footer()).on('keyup change', function () {
  //         if (that.search() !== this['value']) {
  //           that
  //             .search(this['value'])
  //             .draw();
  //         }
  //       });
  //     });
  //   });
  // }

  // private extractData(res: Response) {
  //   const body = res.json();
  //   return body.data || {};
  // }

}