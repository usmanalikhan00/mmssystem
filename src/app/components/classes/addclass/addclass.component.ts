import {Component, ElementRef, ViewChild} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { StudentsService } from  '../../../services/students.services';
import { SubjectsService } from  '../../../services/subjects.services';
import { TeachersService } from  '../../../services/teachers.services';
import { ClassesService } from  '../../../services/classes.services';


@Component({
    selector: 'add-class',
    providers: [FormBuilder,
                SubjectsService,
                TeachersService,
                ClassesService,
                StudentsService],
    templateUrl: 'addclass.html',
    styleUrls: ['addclass.css']
})

export class AddClass {
  
  addClassForm: FormGroup;

  loggedUser: any = null
  alerts: any = []

  dropdownList = [];
  selectedItems = [];

  allSubjects: any = []
  allTeachers: any = []
  allStudents: any = []

  dropdownSettings = {};
  dropdownStudent = {};
  dropdownTeacher = {};
  dropdownSubject = {};
  dropdownSettingsSingle = {};

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _subjectsService: SubjectsService,
              private _teachersService: TeachersService,
              private _classesService: ClassesService,
              private _router: Router) {

    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("CLASSES COMPONENT STARTED:--\n", this.loggedUser)
    this._buildAddClassForm()
  
  }
  

  private _buildAddClassForm(){
    this.addClassForm = this._formBuilder.group({
      name: ['', Validators.required],
      section: ['', Validators.required],
      teacher: ['', Validators.required],
      subject: ['', Validators.required],
      students: ['', Validators.required],
    });
  }

  ngOnInit(){
    this.setForm()
  }

  submitClass(values){
    values.schoolId = this.loggedUser.schoolId
    values.teacher = values.teacher[0].id
    values.subject = values.subject[0].id
    var studentIds = []
    for (let val of values.students){
      studentIds.push(val.id)
    }
    values.students = studentIds
    console.log("VALUES FROM CLASS FORM:---\n", values)
    this._classesService.addClass(values)
    .subscribe(result => {
      console.log("RESULT FROM ADD CLASS API:---'\n", result)
      if (result.status){

        this.addClassForm.reset()
        // (<FormGroup>this.addClassForm).setValue({'subject': '', 
        //                                            'teacher': '', 
        //                                            'name': '', 
        //                                            'section': '', 
        //                                            'students': '',}, {onlySelf: true})
        this.alerts = []
        this.alerts.push({
          type: 'success',
          msg: `Class Added Successfully!!`,
          timeout: 7000
        });
      }else{
        this.alerts = []
        this.alerts.push({
          type: 'danger',
          msg: `Something Went Wrong!!`,
          timeout: 7000
        });
      }
    })

  }


  setForm(){
    this._subjectsService.allSubjectsBySchool(this.loggedUser.schoolId)
    .subscribe(subjects => {
      console.log("RESULT FROM SCHOOL SUBJECTS:---\n", subjects)
      this.allSubjects = subjects.subjects
      this.dropdownSubject = {
        singleSelection: true,
        idField: 'id',
        textField: 'title',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
      this._teachersService.allTeachersBySchool(this.loggedUser.schoolId)
      .subscribe(teachers => {
        console.log("RESULT FROM SCHOOL TEACHERS:---\n", teachers)
        this.allTeachers = teachers.teachers
        this.dropdownTeacher = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: true
        };
        this._studentsService.allStudentsBySchool(this.loggedUser.schoolId)
        .subscribe(students => {
          console.log("RESULT FROM SCHOOL STUDENTS:---\n", students)
          this.allStudents = students.students
          this.dropdownStudent = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
          };
        })
      })
    })
  }

  goBack(){
    this._router.navigate(['/classes'])
  }

  onItemSelect (item:any) {
    console.log(item);
  }

  onSelectAll (items: any) {
    console.log(items);
  }  

}