import {Component, ElementRef, ViewChild} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { StudentsService } from  '../../../services/students.services';
import { SubjectsService } from  '../../../services/subjects.services';
import { TeachersService } from  '../../../services/teachers.services';
import { ClassesService } from  '../../../services/classes.services';

import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';

@Component({
    selector: 'edit-class',
    providers: [FormBuilder,
                SubjectsService,
                TeachersService,
                ClassesService,
                StudentsService],
    templateUrl: 'editclass.html',
    styleUrls: ['editclass.css']
})

export class EditClass {
  
  addClassForm: FormGroup;

  loggedUser: any = null
  classToEdit: any = null
  classId: any = null
  alerts: any = []

  dropdownList = [];
  selectedItems = [];

  allSubjects: any = []
  allTeachers: any = []
  allStudents: any = []

  selectedStudents: any = []
  selectedTeacher: any = []
  selectedSubject: any = []

  dropdownSettings = {};
  dropdownStudent = {};
  dropdownTeacher = {};
  dropdownSubject = {};
  dropdownSettingsSingle = {};

  constructor(private _formBuilder: FormBuilder, 
              private _studentsService: StudentsService,
              private _subjectsService: SubjectsService,
              private _teachersService: TeachersService,
              private _classesService: ClassesService,
              private _activatedRouter: ActivatedRoute,
              private _router: Router) {

    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("CLASSES COMPONENT STARTED:--\n", this.loggedUser)
    this._buildAddClassForm()
  
  }
  

  private _buildAddClassForm(){
    this.addClassForm = this._formBuilder.group({
      name: ['', Validators.required],
      section: ['', Validators.required],
      teacher: ['', Validators.required],
      subject: ['', Validators.required],
      students: ['', Validators.required],
    });
  }

  ngOnInit(){
    this._activatedRouter.params.subscribe(params => {
      this.classId = params['classId']; // (+) converts string 'id' to a number
      this._classesService.getClassById(this.classId)
      .subscribe(result => {
        this.classToEdit = result.classObject
        console.log("CLASS TO EDIT:-----\n", this.classToEdit)
        this.setForm()
      })
    });
  }

  submitClass(values){
    values.classId = this.classToEdit.id
    values.isActive = this.classToEdit.isActive
    values.teacher = values.teacher[0].id
    values.subject = values.subject[0].id
    var studentIds = []
    for (let val of values.students){
      studentIds.push(val.id)
    }
    values.students = studentIds
    console.log("VALUES FROM CLASS FORM:---\n", values)
    this._classesService.editClass(values)
    .subscribe(result => {
      console.log("RESULT FROM EDIT CLASS API:---'\n", result)
      if (result.status){

        this.alerts = []
        this.alerts.push({
          type: 'success',
          msg: `Class Updated Successfully!!`,
          timeout: 7000
        });
        // this.addClassForm.reset()
      }else{

        this.alerts = []
        this.alerts.push({
          type: 'danger',
          msg: `Something Went Wrong!!`,
          timeout: 7000
        });

      }
    })

  }


  setForm(){
    this._subjectsService.allSubjectsBySchool(this.loggedUser.schoolId)
    .subscribe(subjects => {
      // console.log("RESULT FROM SCHOOL SUBJECTS:---\n", subjects)
      this.allSubjects = subjects.subjects
      this.selectedSubject.push(this.classToEdit.subject)
      this.dropdownSubject = {
        singleSelection: true,
        idField: 'id',
        textField: 'title',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
      this._teachersService.allTeachersBySchool(this.loggedUser.schoolId)
      .subscribe(teachers => {
        // console.log("RESULT FROM SCHOOL TEACHERS:---\n", teachers)
        this.allTeachers = teachers.teachers
        this.selectedTeacher.push(this.classToEdit.teacher) 
        this.dropdownTeacher = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: true
        };
        this._studentsService.allStudentsBySchool(this.loggedUser.schoolId)
        .subscribe(students => {
          // console.log("RESULT FROM SCHOOL STUDENTS:---\n", students)
          this.allStudents = students.students
          this.selectedStudents = this.classToEdit.students
          this.dropdownStudent = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
          };
          (<FormGroup>this.addClassForm).patchValue({'teacher': this.selectedTeacher, 
                                                     'subject': this.selectedSubject,
                                                     'name': this.classToEdit.name,
                                                     'section': this.classToEdit.section,}, {onlySelf: true})
        })
      })
    })
  }

  goBack(){
    this._router.navigate(['/classes'])
  }

  onItemSelect (item:any) {
    console.log(item);
  }

  onSelectAll (items: any) {
    console.log(items);
  }  

}