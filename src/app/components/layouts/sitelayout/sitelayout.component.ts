import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'site-layout',
    providers: [FormBuilder],
    templateUrl: 'sitelayout.html',
    styleUrls: ['sitelayout.css']
})

export class SiteLayout {
  
  constructor(private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    var self = this;
    // console.log("SITE LAYOUT STARTED")
  }

}