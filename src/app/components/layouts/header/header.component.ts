import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { SchoolService } from  '../../../services/schools.services';
import { InboxService } from  '../../../services/inbox.service';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
    selector: 'app-header',
    providers: [FormBuilder, SchoolService, InboxService],
    templateUrl: 'header.html',
    styleUrls: ['header.css']
})

export class Header {
  
  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService,
              private _inboxService: InboxService,
              private _router: Router) {
  }

  toggle: boolean = false
  loggedUser: any = null
  allChats: any = []
  isReadCount: any = 0



  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("LOGGED USER:--", this.loggedUser)
    this.getAllChats(this.loggedUser.id)
  }

  getAllChats(userId){
    this._inboxService.allChatsByUserId(userId)
    .subscribe(result => {
      this.allChats = result.conversations
      var sum = 0
      for (let chat of this.allChats){
        chat.date = moment.utc(chat.messages[0].dateCreated).local().fromNow()
        if (this.loggedUser.school.userId === chat.messages[0].senderId){
          this._inboxService.parentByUserId(chat.messages[0].receiverId)
          .subscribe(result => {
            // console.log("USER NAME TO SHOW IN CHAT:---\n", result)
            chat.parent = result.student  
          })
        }
        if (this.loggedUser.school.userId === chat.messages[0].receiverId){
          this._inboxService.parentByUserId(chat.messages[0].senderId)
          .subscribe(result => {
            chat.parent = result.student  
          })
        }
        if (!chat.isRead){
          sum = sum + 1
        }
      }
      this.isReadCount = sum
      // console.log("ALL CHATS FOR SCHOOL:---\n", this.allChats)
    })
  }

  navbarClicked(){
    if (this.toggle)
      (document.querySelector('#navigation') as HTMLElement).style.display="none"
    else
      (document.querySelector('#navigation') as HTMLElement).style.display="block"
    this.toggle = !this.toggle
  }

    // (document.querySelector('#navigation') as HTMLElement).style.display="none"
  logout(){
    localStorage.removeItem('user')
    console.log("USER LOGGED OUT:---", localStorage.getItem('user'))
    this._router.navigate(['/'])
  }

  changePassword(){
    this._router.navigate(['/changepassword'])
  }

  ngAfterViewInit(){
  }

}