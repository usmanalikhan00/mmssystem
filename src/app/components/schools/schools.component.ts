import {Component, ElementRef, TemplateRef, ViewChild} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { SchoolService } from '../../services/schools.services'

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

declare var $:any

@Component({
    selector: 'schools',
    providers: [FormBuilder, SchoolService],
    templateUrl: 'schools.html',
    styleUrls: ['schools.css']
})


export class Schools {
  
  public errorMsg = '';

  toggle: boolean = false

  allSchools: any = []
  
  alerts: any = []
  loggedUser: any = null

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService, 
              private _router: Router) {
  }

  ngOnInit(){
    var self = this;
    // $("#basic-form").on('submit' , submitForm());
    this.loggedUser = JSON.parse(localStorage.getItem('user')) 
    console.log("SCHOOL PAGE STARTED:---\n", this.loggedUser)
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.getAllSchools()
  }
  
  getAllSchools(){
    // this.loading = true
    this._schoolService.allSchools()
    .subscribe(schools => {
      console.log("***** ALL SCHOOL *****", schools)
      this.allSchools = schools.schools
      // this.loading = false
      this.dtTrigger.next()
    })
  }

  editSchool(school){
    this._router.navigate(['/editschool', school.id])
  }

  showAddForm(){
    // this.addFlag = true
    this._router.navigate(['/addschool'])
  }

  goBack(){
    this._router.navigate(['/dashboard'])
  }
  
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}