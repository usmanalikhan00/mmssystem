import {Component, ElementRef, TemplateRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { SchoolService } from '../../../services/schools.services'

declare var $:any

@Component({
    selector: 'add-school',
    providers: [FormBuilder, SchoolService],
    templateUrl: 'addschool.html',
    styleUrls: ['addschool.css']
})


export class AddSchool {
  
  public errorMsg = '';
  addSchoolForm: FormGroup;

  toggle: boolean = false
  
  alerts: any = []
  loggedUser: any = null


  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService, 
              private _router: Router) {
    this._buildAddSchoolForm();
  }

  private _buildAddSchoolForm(){
    this.addSchoolForm = this._formBuilder.group({
      name: ['', Validators.required],
      branch: ['', Validators.required],
      address: ['', Validators.required],
      email: [''],
      web: [''],
      phone: [''],
      
      cpname: ['', Validators.required],
      cpaddress: ['', Validators.required],
      cpdesignation: [''],
      cpemail: [''],
      cpmobile: [''],
      cpphone: [''],
      
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

  }

  ngOnInit(){
    var self = this;
    console.log("SCHOOL PAGE STARTED")
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
  }
  
  // editSchool(school){
  //   this.schoolToEdit = school
  //   console.log("SCHOOL TO EDIT:---", this.schoolToEdit)
  //   var newSchool = {'name': school.name,
  //                   'branch': school.branch,
  //                   'address': school.address,
  //                   'email': school.email,
  //                   'web': school.web,
  //                   'phone': school.phone,
  //                   'cpname': school.schoolContactPerson.name,
  //                   'cpaddress': school.schoolContactPerson.address,
  //                   'cpdesignation': school.schoolContactPerson.designation,
  //                   'cpemail': school.schoolContactPerson.email,
  //                   'cpmobile': school.schoolContactPerson.mobile,
  //                   'cpphone': school.schoolContactPerson.phone,
  //                   'username': "none",
  //                   'password': "none",
  //                 };
  //   (<FormGroup>this.addSchoolForm).setValue(newSchool, {onlySelf:true})
  //   this.editFlag = true
  //   this.addFlag = true
  // }

  submitSchool(values){
    // if (!this.editFlag){
    console.log("ADD SCHOOL FORM VALUES:---", values)
    this._schoolService.addSchool(values)
    .subscribe(result => {
      console.log("RESULT FROM CREATE:--", result)
      this.alerts = []
      this.alerts.push({
        type: 'info',
        msg: `School Added Successfully!!`,
        timeout: 4000
      });
      this.addSchoolForm.reset()
    })
    // }else{
    //   values.schoolId = this.schoolToEdit.id
    //   values.contactPersonId = this.schoolToEdit.contactPersonId
    //   values.teachersAllowed = this.schoolToEdit.teachersAllowed
    //   values.parentsAllowed = this.schoolToEdit.parentsAllowed
    //   values.isActive = true
    //   console.log("VALUES TO SAVE AFTER EDIT:---", values)
      
    // }
  }

  reset(){
    this.addSchoolForm.reset()
  }

  goBack(){
    this._router.navigate(['/schools'])
  }

}