import {Component, ElementRef, TemplateRef} from '@angular/core';
import {Location} from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { SchoolService } from '../../../services/schools.services'
import { RouterService } from '../../../services/routing.service';
declare var $:any

@Component({
    selector: 'edit-school',
    providers: [FormBuilder, SchoolService, RouterService],
    templateUrl: 'editschool.html',
    styleUrls: ['editschool.css']
})


export class EditSchool {
  
  public errorMsg = '';
  addSchoolForm: FormGroup;

  toggle: boolean = false
  
  alerts: any = []
  loggedUser: any = null
  schoolId: any = null
  schoolToEdit: any = null
  image: any = null


  constructor(private _formBuilder: FormBuilder, 
              private _schoolService: SchoolService, 
              private _activatedRouter: ActivatedRoute, 
              private _location: Location, 
              private _routerService: RouterService, 
              private _router: Router) {
    this._buildAddSchoolForm();
  }

  private _buildAddSchoolForm(){
    this.addSchoolForm = this._formBuilder.group({
      name: ['', Validators.required],
      branch: ['', Validators.required],
      address: ['', Validators.required],
      email: [''],
      web: [''],
      phone: [''],
      
      cpname: ['', Validators.required],
      cpaddress: ['', Validators.required],
      cpdesignation: [''],
      cpemail: [''],
      cpmobile: [''],
      cpphone: [''],
      
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

  }

  ngOnInit(){
    var self = this;
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    // console.log("PREVIOUS URL:---\n", this._routerService.getPreviousUrl(), document.referrer)
    this._activatedRouter.params.subscribe(params => {
      this.schoolId = params['schoolId']; // (+) converts string 'id' to a number
      this._schoolService.getSchoolById(this.schoolId)
      .subscribe(result => {
        this.schoolToEdit = result.school
        if (this.schoolToEdit.imageUrl)
          this.image = this.schoolToEdit.imageUrl
        console.log("SCHOOL EDIT PAGE STARTED:----\n", this.loggedUser, "\n", result, "\n", this.schoolToEdit)
        var newSchool = {'name': this.schoolToEdit.name,
                        'branch': this.schoolToEdit.branch,
                        'address': this.schoolToEdit.address,
                        'email': this.schoolToEdit.email,
                        'web': this.schoolToEdit.web,
                        'phone': this.schoolToEdit.phone,
                        'cpname': this.schoolToEdit.schoolContactPerson.name,
                        'cpaddress': this.schoolToEdit.schoolContactPerson.address,
                        'cpdesignation': this.schoolToEdit.schoolContactPerson.designation,
                        'cpemail': this.schoolToEdit.schoolContactPerson.email,
                        'cpmobile': this.schoolToEdit.schoolContactPerson.mobile,
                        'cpphone': this.schoolToEdit.schoolContactPerson.phone,
                        'username': "none",
                        'password': "none",
                      };
        (<FormGroup>this.addSchoolForm).setValue(newSchool, {onlySelf:true})
      })
    });
  }
  

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      console.log(myReader.result.toString().replace("data:image/jpeg;base64,", ""));
    }
    myReader.readAsDataURL(file);
  }

  submitSchool(values){
    values.schoolId = this.schoolToEdit.id
    values.contactPersonId = this.schoolToEdit.contactPersonId
    values.teachersAllowed = this.schoolToEdit.teachersAllowed
    values.parentsAllowed = this.schoolToEdit.parentsAllowed
    values.isActive = true
    values.schoolImage = this.image ? this.image.toString().replace("data:image/jpeg;base64,", "") : null
    console.log("VALUES TO SAVE AFTER EDIT:---", values)
    this._schoolService.editSchool(values)
    .subscribe(result => {
      this.alerts = []
      this.alerts.push({
        type: 'success',
        msg: `School Updated Successfully!!`,
        timeout: 2800
      });
      if (result.status){
        console.log("RESULT FROM EDIT STDUENT:--", result)
        window.location.reload()
      }
    })
  }

  reset(){
    this.addSchoolForm.reset()
    this.image = null
  }

  goBack(){
    // this._router.navigate(['/schools'])
    this._location.back()
  }

}