export { Login } from './login/login.component';
export { Dashboard } from './dashboard/dashboard.component';
export { Header } from './layouts/header/header.component';
export { SiteLayout } from './layouts/sitelayout/sitelayout.component';
export { Schools } from './schools/schools.component';
export { AddSchool } from './schools/addschool/addschool.component';
export { EditSchool } from './schools/editschool/editschool.component';
// TEACHER
export { Teachers } from './teachers/teachers.component';
export { AddTeacher } from './teachers/addteacher/addteacher.component';
export { EditTeacher } from './teachers/editteacher/editteacher.component';
// STUDENTS
export { Students } from './students/students.component';
export { AddStudent } from './students/addstudent/addstudent.component';
export { EditStudent } from './students/editstudent/editstudent.component';
export { Parents } from './parents/parents.component';
export { Classes } from './classes/classes.component';
export { AddClass } from './classes/addclass/addclass.component';
export { EditClass } from './classes/editclass/editclass.component';
// FEE SLIP
export { Feeslips } from './feeslips/feeslips.component';
export { Addfeeslip } from './feeslips/addfeeslip/addfeeslip.component';
export { Editfeeslip } from './feeslips/editfeeslip/editfeeslip.component';
// SUBJECTS
export { Subjects } from './subjects/subjects.component';
export { AddSubject } from './subjects/addsubject/addsubject.component';
export { EditSubject } from './subjects/editsubject/editsubject.component';
// PLANNER
export { Planner } from './planner/planner.component';
export { EditPlanner } from './planner/editplanner/editplanner.component';
// SETTINGS
export { Settings } from './settings/settings.component';
export { ChangePassword } from './settings/changepassword/changepassword.component';
// INBOX
export { Inbox } from './inbox/inbox.component';
export { SingleChat } from './singlechat/singlechat.component';
// PIPES
export { CapFilterPipe } from './shared/capatalize-filter.pipe';