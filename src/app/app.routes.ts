import { Routes, RouterModule } from '@angular/router';
import { Login, 
         Dashboard, 
         SiteLayout, 
         Schools, 
         AddSchool, 
         EditSchool, 
         Teachers, 
         AddTeacher, 
         EditTeacher, 
         Students, 
         AddStudent, 
         EditStudent, 
         Subjects, 
         AddSubject, 
         EditSubject, 
         Classes, 
         AddClass, 
         EditClass, 
         Feeslips, 
         Addfeeslip, 
         Editfeeslip, 
         Planner, 
         EditPlanner, 
         Settings, 
         ChangePassword, 
         Inbox, 
         SingleChat, 
         Parents } from "../app/components/component-index";
import { AuthGuard } from './_gaurds/auth.gaurd';

export const ROUTES: Routes = [

    //App routes goes here 
    { 
      path: '', 
      component: Login,
    },
    { 
      path: 'login', 
      component: Login,
    },
    { 
      path: 'changepassword', 
      component: ChangePassword, 
      canActivate: [AuthGuard]
    },

    // Site routes goes here here
    { 
        path: '', 
        component: SiteLayout,
        children: [
          { path: 'dashboard', 
            component: Dashboard, 
            canActivate: [AuthGuard]
          },
          { path: 'schools', 
            component: Schools, 
            canActivate: [AuthGuard]
          },
          { path: 'addschool', 
            component: AddSchool, 
            canActivate: [AuthGuard]
          },
          { path: 'editschool/:schoolId', 
            component: EditSchool, 
            canActivate: [AuthGuard]
          },

          // TEACHERS
          { path: 'teachers', 
            component: Teachers, 
            canActivate: [AuthGuard]
          },
          { path: 'addteacher', 
            component: AddTeacher, 
            canActivate: [AuthGuard]
          },
          { path: 'editteacher/:teacherId', 
            component: EditTeacher, 
            canActivate: [AuthGuard]
          },
          // STUDENT ROUTES
          { path: 'students', 
            component: Students, 
            canActivate: [AuthGuard]
          },
          { path: 'addstudent', 
            component: AddStudent, 
            canActivate: [AuthGuard]
          },
          { path: 'editstudent/:studentId', 
            component: EditStudent, 
            canActivate: [AuthGuard]
          },

          { path: 'parents', 
            component: Parents, 
            canActivate: [AuthGuard]
          },
          // SUBJECTS
          { path: 'subjects', 
            component: Subjects, 
            canActivate: [AuthGuard]
          },
          { path: 'addsubject', 
            component: AddSubject, 
            canActivate: [AuthGuard]
          },
          { path: 'editsubject/:subjectId', 
            component: EditSubject, 
            canActivate: [AuthGuard]
          },
          // CLASSES
          { path: 'classes', 
            component: Classes, 
            canActivate: [AuthGuard]
          },
          { path: 'addclass', 
            component: AddClass, 
            canActivate: [AuthGuard]
          },
          { path: 'editclass/:classId', 
            component: EditClass, 
            canActivate: [AuthGuard]
          },
          // FEE SLIPS
          { path: 'feeslips', 
            component: Feeslips, 
            canActivate: [AuthGuard]
          },
          { path: 'addfeeslip', 
            component: Addfeeslip, 
            canActivate: [AuthGuard]
          },
          { path: 'editfeeslip/:feeslipId', 
            component: Editfeeslip, 
            canActivate: [AuthGuard]
          },          
          // PLANNER
          { path: 'planner', 
            component: Planner, 
            canActivate: [AuthGuard]
          },
          { path: 'editplanner/:plannerId', 
            component: EditPlanner, 
            canActivate: [AuthGuard]
          },
          // SETTINGS
          { path: 'settings', 
            component: Settings, 
            canActivate: [AuthGuard]
          },
          // Inbox
          { path: 'inbox', 
            component: Inbox, 
            canActivate: [AuthGuard]
          },
          { path: 'singleinbox', 
            component: SingleChat, 
            canActivate: [AuthGuard]
          },
        ]
    },

    //no layout routes
    

    // otherwise redirect to home
    { path: '**', 
      redirectTo: '' 
    }
  

];
