import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AppComponent } from './app.component';
import { TabsModule, AlertModule, BsDatepickerModule, ModalModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from './services/login.services';
import { AuthGuard } from './_gaurds/auth.gaurd';
import { NgxLoadingModule } from 'ngx-loading';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AgGridModule } from 'ag-grid-angular';
import { DataTablesModule } from 'angular-datatables';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FlatpickrModule } from 'angularx-flatpickr';

import { Login, 
         Dashboard, 
         Header, 
         SiteLayout, 
         Schools, 
         AddSchool, 
         EditSchool, 
         Teachers, 
         AddTeacher, 
         EditTeacher, 
         Students, 
         AddStudent, 
         EditStudent, 
         Subjects, 
         AddSubject, 
         EditSubject, 
         Classes, 
         AddClass, 
         EditClass, 
         Feeslips, 
         Addfeeslip, 
         Editfeeslip, 
         Planner, 
         EditPlanner, 
         Settings, 
         ChangePassword, 
         Inbox, 
         CapFilterPipe, 
         SingleChat, 
         Parents } from "./components/component-index";
         
@NgModule({
  declarations: [
    AppComponent,
    Login,
    Dashboard,
    Header,
    SiteLayout,
    Schools,
    AddSchool,
    EditSchool,
    Teachers,
    AddTeacher,
    EditTeacher,
    Students,
    AddStudent,
    EditStudent,
    Parents,
    Subjects,
    AddSubject,
    EditSubject,
    Classes,
    AddClass,
    EditClass,
    Feeslips,
    Addfeeslip,
    Editfeeslip,
    Planner,
    EditPlanner,
    Settings,
    ChangePassword,
    Inbox,
    CapFilterPipe,
    SingleChat,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    FlatpickrModule.forRoot(),
    ModalModule.forRoot(),
    RouterModule.forRoot(ROUTES, {useHash: true}),
    FormsModule, 
    ReactiveFormsModule,
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxLoadingModule.forRoot({}),
    HttpModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    DataTablesModule, 
  ],
  providers: [LoginService, AuthGuard],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
