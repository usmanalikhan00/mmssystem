import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PlannerService{

  constructor(private http: Http, 
              private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
  }

  addPlanner(planner){
    var newPlanner = {'title':planner.title, 
                      'schoolId':planner.schoolId,
                      'classId':planner.classId,
                      'startDate':planner.startDate,
                      'endDate':planner.endDate,
                      'type':planner.type}
    // console.log("USER ID FROM AUTH USER: ", userId);
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/planners/create', newPlanner)
        .map(res => res.json());
  }
  
  editPlanner(planner){
    var newPlanner = {'plannerId':planner.plannerId, 
                      'title':planner.title,
                      'classId':planner.classId,
                      'startDate':planner.startDate,
                      'endDate':planner.endDate,
                      'type':planner.type}
    // console.log("USER ID FROM AUTH USER: ", userId);
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/planners/update', newPlanner)
        .map(res => res.json());
  }




  getPlannerById(plannerId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/planners/get/'+plannerId)
          .map(res => res.json());
  }

  getPlannersBySchoolId(SchoolId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/planners/get/school/'+SchoolId)
          .map(res => res.json());
  }

}