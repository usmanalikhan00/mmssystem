import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SubjectsService{

  constructor(private http: Http, 
              private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
  }

  allSubjects(schoolId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/subjects/get/school/'+schoolId)
        .map(res => res.json());
  }

  addSubject(subject){
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/subjects/create', {'schoolId': subject.schoolId,'title': subject.title})
        .map(res => res.json());
  }


  editSubject(subject){
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/subjects/update', {'subjectId': subject.subjectId,'title': subject.title})
        .map(res => res.json());
  }


  allSubjectsBySchool(schoolId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/subjects/get/school/'+schoolId)
        .map(res => res.json());
  }

  getSubjectsById(subjectId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/subjects/get/'+subjectId)
        .map(res => res.json());
  }

  allSubjectsCountBySchool(schoolId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/subjects/count/school/'+schoolId)
        .map(res => res.json());
  }


}