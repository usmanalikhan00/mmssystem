import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService{
  
    constructor(private http: Http, 
                private _router: Router, 
                private _httpClient: HttpClient, 
                private _compiler: Compiler){
    }
    
    getRolesNav(){
        // var userRole = localStorage.getItem('userrole')
        return this.http.get("https://pitbmed.herokuapp.com/api/getnavbar")
                .map(res => res.json());
    }

    getLoggedInUser(username, password){
      let headers = new Headers({ 
        'Content-Type': 'application/x-www-form-urlencoded', 
        'Accept':'application/json, text/plain, */*'
      });
      let options = new RequestOptions({ headers: headers });
      // let body = new HttpParams()
      //     .set('username', username)
      //     .set('password', password);
      // console.log("Login service called", body);
      return this.http.post('https://mmschoolapp.azurewebsites.net/api/users/login', 
                            {"username": username, "password": password})
          .map(res => res.json());
    }

    getAuthUser(userId){
        console.log("USER ID FROM AUTH USER: ", userId);
        return this.http.get('https://pitbmed.herokuapp.com/api/user/'+userId)
            .map(res => res.json());
    }

    loggedIn(){
        if (localStorage.getItem("user") === null){
          return false
        }
        return true
    }
    
    logout() {
        this._compiler.clearCache();
        localStorage.removeItem("userid");
        localStorage.removeItem("userrole");
        this._router.navigate(['']);
    }
}