import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SchoolService{
  constructor(private http: Http, 
              private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
  }

  addSchool(school){
    let headers = new Headers({ 
      'Content-Type': 'application/x-www-form-urlencoded', 
      'Accept':'application/json, text/plain, */*'
    });
    let options = new RequestOptions({ headers: headers });
    let body = new HttpParams()
        .set('schoolName', school.name)
        .set('schoolAddress', school.address)
        .set('schoolPhone', school.phone)
        .set('schoolEmail', school.email)
        .set('schoolWeb', school.web)
        .set('schoolBranch', school.branch)
        .set('contactName', school.cpname)
        .set('contactDesignation', school.cpdesignation)
        .set('contactAddress', school.cpaddress)
        .set('contactPhone', school.cpphone)
        .set('contactEmail', school.cpemail)
        .set('contactMobile', school.cpmobile)
        .set('teachersAllowed', "5")
        .set('parentsAllowed', "10")
        .set('username', school.username)
        .set('password', school.password);
    // console.log("Login service called", body);
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/schools/create', body.toString(), options)
        .map(res => res.json());
  }

  editSchool(school){
    let headers = new Headers({ 
      'Content-Type': 'application/x-www-form-urlencoded', 
      'Accept':'application/json, text/plain, */*'
    });
    let options = new RequestOptions({ headers: headers });
    let body = {
      'schoolId': school.schoolId,
      'schoolName': school.name,
      'schoolBranch': school.branch,
      'schoolAddress': school.address,
      'schoolPhone': school.phone,
      'schoolEmail': school.email,
      'schoolWeb': school.web,
      'teachersAllowed': school.teachersAllowed,
      'parentsAllowed': school.parentsAllowed,
      'schoolImage': school.schoolImage,
      'isActive': school.isActive,
      'contactPersonId': school.contactPersonId,
      'contactName': school.cpname,
      'contactDesignation': school.cpdesignation,
      'contactPhone': school.cpphone,
      'contactMobile': school.cpmobile,
      'contactEmail': school.cpemail,
      'contactAddress': school.cpaddress,
    }
    // console.log("Login service called", body);
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/schools/update', body)
        .map(res => res.json());
  }



  allSchools(){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/schools/get/all')
          .map(res => res.json());
  }



  getSchoolById(schoolId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/schools/get/'+schoolId)
          .map(res => res.json());
  }



  allSchoolsCount(){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/schools/count')
          .map(res => res.json());
  }

  resetPassword(userId, password){
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/users/password/change', {'userId': userId, 'newPassword': password})
        .map(res => res.json());
  }


}