import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class FeeslipsService{

  constructor(private http: Http, 
              private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
  }

  allFeeSlips(feeslipId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/subjects/get/school/'+feeslipId)
        .map(res => res.json());
  }

  addFeeSlip(feeslip){
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/feeslips/create', 
                          {"schoolId": feeslip.schoolId,
                            "studentId": feeslip.student[0].id,
                            "feeSlipFields": feeslip.feeSlipFields,
                            "submissionDate": feeslip.submitdate.toISOString(),
                            "studentNationality": feeslip.nationality,
                            "isRecurring": feeslip.recurring,
                            "recurringDay": feeslip.recurringdate,
                            "totalAmount": feeslip.amount,
                            "title": feeslip.title,
                            "status": true,
                            "tax": feeslip.tax+'%'
                          })
        .map(res => res.json());
  }


  editFeeSlip(feeslip){
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/feeslips/update', 
                          {"schoolId": feeslip.schoolId,
                            "feeSlipId": feeslip.feeSlipId,
                            "studentId": feeslip.student[0].id,
                            "feeSlipFields": feeslip.feeSlipFields,
                            "submissionDate": feeslip.submitdate.toISOString(),
                            "studentNationality": feeslip.nationality,
                            "isRecurring": feeslip.recurring,
                            "recurringDay": feeslip.recurringdate,
                            "totalAmount": feeslip.amount,
                            "title": feeslip.title,
                            "status": true,
                            "tax": feeslip.tax+'%'
                          })
        .map(res => res.json());
  }


  allFeeSlipsBySchool(schoolId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/feeslips/get/school/'+schoolId)
        .map(res => res.json());
  }

  getFeeSlipById(feeslipId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/feeslips/get/'+feeslipId)
        .map(res => res.json());
  }

  allFeeSlipsCountBySchool(schoolId){
    return this.http.get('https://mmschoolapp.azurewebsites.net/api/feeslips/count/school/'+schoolId)
        .map(res => res.json());
  }


}

