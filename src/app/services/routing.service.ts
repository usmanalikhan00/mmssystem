import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router, NavigationEnd  } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class RouterService{

  history: any = []
  private previousUrl: string;
  private currentUrl: string;

  constructor(private http: Http, 
              private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
      this.currentUrl = this._router.url;
      _router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {        
          this.previousUrl = this.currentUrl;
          this.currentUrl = event.url;
        };
      });
  }

  public getPreviousUrl() {
    return this.previousUrl;
  } 
}