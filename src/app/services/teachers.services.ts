import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TeachersService{
  constructor(private http: Http, 
              private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
  }

  addTeacher(teacher){
    let headers = new Headers({ 
      'Content-Type': 'application/x-www-form-urlencoded', 
      'Accept':'application/json, text/plain, */*'
    });
    let options = new RequestOptions({ headers: headers });
    let newTeacher = {
      'schoolId': teacher.schoolId,
      'passportNo': teacher.passportno,
      'joiningDate': teacher.joindate,
      'name': teacher.name,
      'teacherImage': teacher.teacherImage,
      'username': teacher.username,
      'password': teacher.password,
    }
    // console.log("ADD TEACHER SERVICE:---\n", body.toString());
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/teachers/create', newTeacher)
        .map(res => res.json());
  }

  editTeacher(teacher){
    let headers = new Headers({ 
      'Content-Type': 'application/x-www-form-urlencoded', 
      'Accept':'application/json, text/plain, */*'
    });
    let options = new RequestOptions({ headers: headers });
    let body = new HttpParams()
        .set('teacherId', teacher.teacherId)
        .set('passportNo', teacher.passportno)
        .set('joiningDate', teacher.joindate)
        .set('isActive', teacher.isActive)
        .set('name', teacher.name);
    console.log("EDIT TEACHER SERVICE:---\n", body.toString());
    return this.http.post('https://mmschoolapp.azurewebsites.net/api/teachers/update', body.toString(), options)
        .map(res => res.json());
  }



  allTeachers(){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/teachers/get/all')
          .map(res => res.json());
  }

  allTeachersBySchool(schoolId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/teachers/get/all/'+schoolId)
          .map(res => res.json());
  }



  allTeachersCount(){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/teachers/count')
          .map(res => res.json());
  }

  getTeacherById(teacherId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/teachers/get/'+teacherId)
          .map(res => res.json());
  }

  allTeachersCountBySchool(schoolId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/teachers/count/'+schoolId)
          .map(res => res.json());
  }





}