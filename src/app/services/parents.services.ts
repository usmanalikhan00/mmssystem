import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ParentsService{

  constructor(private http: Http, 
              private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
  }

  allParents(){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/parents/get/all')
          .map(res => res.json());
  }


  allParentsCount(){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/parents/count')
          .map(res => res.json());
  }

}